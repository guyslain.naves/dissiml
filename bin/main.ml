open DissimilarityLib
open CopointLib

(* let _list = 
 *   [ 0; 3; 4; 1;
 *     3; 0; 1; 2;
 *     4; 1; 0; 3;
 *     1; 2; 3; 0 
 *   ]
 * 
 * 
 * let print_ordered_matrix (module D : Dissimilarity.D with type t = int) order = 
 *   List.iter (fun i -> 
 *       List.iter (fun j -> 
 *           Printf.printf "%d " (D.d i j)
 *         )
 *         order;
 *       Printf.printf "\n"
 *     ) 
 *     order *)




let _from_stdin algo =  
  match IO.Plain.from_stdin () with 
  | None -> Printf.printf "Parsing error"
  | Some diss ->
     let order = algo diss in
     let pp_sep fmt () =  Format.pp_print_string fmt ", " in
     Format.printf "Order : %a\n%!" 
       Format.(pp_print_option (pp_print_list ~pp_sep pp_print_int))
       order;
     Option.iter (fun elements -> 
         Format.printf "Matrix:@,@[<v 2>%a@]\n%!"
           IO.Plain.pretty_printer 
           Dissimilarity.({ diss with elements })
       )
       order;  
     Format.printf "%!";
     exit 0



let _main = 
  let _algo1 diss = RobinsonCCNP.find_compatible_order diss in 
  let _algo2 diss = ImperativeCCNP.find_compatible_order diss |> Option.map Array.to_list in 
  let _algo3 diss = Recognition.Pivotpair.algo diss (fun i -> i) in
  let _algo4 diss = ChainLib.RobinsonByChain.find_compatible_order diss in 
  _from_stdin _algo3
