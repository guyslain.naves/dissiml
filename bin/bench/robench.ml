open! Core_bench
open! CopointLib
open! RandomDissimilarity
open! DissimilarityLib
open! ChainLib

let imperative_copoint_algorithm diss =
  diss
  |> ImperativeCCNP.find_compatible_order
  |> ignore


let functional_copoint_algorithm diss =
  diss 
  |> RobinsonCCNP.find_compatible_order
  |> ignore

let pivotpair_algorithm diss = 
  Recognition.Pivotpair.algo diss (fun i -> i)
  |> ignore

let chain_insertion_algorithm diss = 
  diss 
  |> RobinsonByChain.find_compatible_order 
  |> ignore
  


let toeplitz ~k dim = 
  Toeplitz.toeplitz012 ~n:dim ~k:(min k (dim-2))
    
let incrementing dim = 
  Incrementing.random (Random.get_state ()) dim

let from_pqtree dim = 
  FromPqtree.uniform dim 

let from_intervals ~k dim = 
  FromPqtree.from_intervals ~n:dim ~k

let from_rectangle ~height ~width dim = 
  FromModel.from_points_in_rectangle ~width ~height ~count:dim

let dimensions = [10;100;1000]

type 'a check = Y of 'a  | N of 'a 

let _ = N 0 

let algorithms = 
  [
    Y ("copoint-imp", imperative_copoint_algorithm);
    Y ("copoint-fun", functional_copoint_algorithm);
    Y ("pivotpair", pivotpair_algorithm);
    Y ("chain-insertion", chain_insertion_algorithm)
  ]


let generators =
  [
    Y ("toeplitz1", toeplitz ~k:1);
    Y ("toeplitz2", toeplitz ~k:2);
    Y ("toeplitz5", toeplitz ~k:5);
    Y ("incrementing", incrementing);
    Y ("fromPQtree", from_pqtree);
    Y ("fromIntervals5", from_intervals ~k:5);
    Y ("fromIntervals10", from_intervals ~k:10);
    Y ("fromRectangle100", from_rectangle ~width:100. ~height:10.);
    Y ("fromRectangle1000", from_rectangle ~width:1000. ~height:10.);
    Y ("fromRectangle10000", from_rectangle ~width:10000. ~height:10.);
  ]

let select check_list =
  List.filter_map (function Y a -> Some a | N _ -> None) check_list 

let tests = 
  let (>>=) e f = DataStruct.MoreList.flat_map f e in 
  select generators >>= fun (gen_name, gen) -> 
  select algorithms >>= fun (algo_name, algo) -> 
  [ 
    Bench.Test.create_indexed 
      ~name:(Format.sprintf "%s %s" algo_name gen_name) 
      ~args:dimensions 
      (fun dim ->
        Core.Staged.stage 
          (fun () -> dim |> gen |> Dissimilarity.shuffle |> algo)
      )
  ]

let () = 
  tests
  |> Bench.make_command
  |> Command_unix.run
       
