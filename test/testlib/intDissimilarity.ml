open! DataStruct 
open DissimilarityLib
open Dissimilarity

open QCheck2 


let shrink { elements; d } = 
  if List.length elements <= 1 then Seq.empty 
  else
    elements 
    |> MoreList.extend 
    |> List.to_seq
    |> Seq.map (fun (_,pre,post) -> { elements = List.rev_append pre post; d })




let primitive_gen sampler size = 
  Gen.make_primitive
    ~gen:(fun state -> 
      Random.set_state state;
      sampler size
    )
    ~shrink 


let toeplitz ~k n = 
  primitive_gen 
    (fun size -> RandomDissimilarity.Toeplitz.toeplitz012 ~n:size ~k)
    n


let uniform n = 
  primitive_gen 
    RandomDissimilarity.FromPqtree.uniform
    n 


let from_intervals ~k n = 
  primitive_gen 
    (fun size -> RandomDissimilarity.FromPqtree.from_intervals ~n:size ~k)
    n 

let from_rectangles ~width ~height n =
  primitive_gen 
    (fun size -> RandomDissimilarity.FromModel.from_points_in_rectangle ~width ~height ~count:size)
    n 

let to_string diss = 
  Format.fprintf Format.str_formatter "%a\n" IO.Plain.pretty_printer diss;
  Format.flush_str_formatter ()


