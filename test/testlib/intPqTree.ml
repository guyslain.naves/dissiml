open PqTreeLib
open QCheck2 
open PqTree 


module PrintableInt = 
  struct 
    type t = int 
    let print fmt i = Format.fprintf fmt "%d" i 
  end

module Print = PqTree.MakePrinter(PrintableInt) 

let to_string tree = 
  Format.fprintf Format.str_formatter  "%a" Print.print tree;
  Format.flush_str_formatter ()

module Rnd : FeatCore.RandomSig.S = 
  struct 
    let bits () = Random.bits ()
    let int bound = Random.int bound
  end
module Enum = PqTree.MakeEnumerate(Rnd)

let sized_uniform_gen size random_state = 
  Random.set_state random_state;
  match Enum.sample 1 Enum.enumeration size (size+1) Seq.empty () with
  | Seq.Cons (hd,_) -> hd 
  | _ -> invalid_arg "PQ-tree sized gen called with invalid size"



let pqtree_from_intervals ~n ~k =
  let pqtree = P (DataStruct.MoreList.range 0 (n-1) |> List.map (fun i -> Leaf i)) in 
  let index i = i in 
  let algo = ImperativeBL.get_algorithms pqtree index in
  let insert_interval _ =
    let interval = algo.get () |> PqTree.sample_interval in 
    algo.insert interval |> ignore;
    interval
  in 
  DataStruct.MoreList.range 1 k 
  |> List.map insert_interval 
  |> fun intervals -> (pqtree, intervals)

let param_sized_block_list_gen k n random_state = 
  Random.set_state random_state;
  pqtree_from_intervals ~n ~k 


let block_list_gen k size =
  let open Gen in 
  make_primitive 
    ~gen:(param_sized_block_list_gen k size) 
    ~shrink:(function 
      | (_,[]) -> Seq.empty
      | (pqtree,blocks) -> Seq.cons (pqtree, List.(blocks |> rev |> tl |> rev)) Seq.empty
    )

              

let param_sized_interval_insertion_gen k n random_state = 
  Random.set_state random_state;
  RandomDissimilarity.FromPqtree.pqtree_from_intervals ~n ~k 
  
let sized_interval_insertion_gen k size =
  let open Gen in 
  make_primitive 
    ~gen:(param_sized_interval_insertion_gen k size)
    ~shrink:PqTree.shrink 



let sized_pqtree_gen size =
  let open Gen in 
  oneof 
    [
      make_primitive 
        ~gen:(sized_uniform_gen size)
        ~shrink:PqTree.shrink;
      (int_range 1 10 >>= fun k -> sized_interval_insertion_gen k size)
    ]
  

let pqtree_gen = 
  let open Gen in 
  int_range 1 100 >>= sized_pqtree_gen



let permutation_gen pqtree = 
  let gen random_state = 
    Random.set_state random_state;
    PqTree.sample_permutation pqtree 
  in 
  Gen.make_primitive ~gen ~shrink:(fun _ -> Seq.empty)


let interval_gen pqtree = 
  let gen random_state = 
    Random.set_state random_state;
    PqTree.sample_interval pqtree 
  in 
  let shrink = function 
    | [] -> Seq.empty
    | [_] -> Seq.empty
    | interval -> List.to_seq [ List.tl interval; List.tl (List.rev interval) ]  
  in 
  Gen.make_primitive ~gen ~shrink 


let sized_pqtree_interval_gen size =
  let open Gen in 
  sized_pqtree_gen size >>= fun pqtree -> 
  interval_gen pqtree >>= fun interval -> 
  return (pqtree, interval)


let shuffle_gen pqtree = 
  let gen random_state = 
    Random.set_state random_state;
    PqTree.shuffle pqtree
  in 
  Gen.make_primitive ~gen ~shrink:(fun _ -> Seq.empty)


let sized_shuffled_pair_gen size =
  let open Gen in 
  sized_pqtree_gen size >>= fun pqtree ->
  shuffle_gen pqtree >>= fun shuffle -> 
  return (shuffle, pqtree)


module IntMap = Map.Make(Int)
module CheckPermutation = PqTree.MakeCheckPermutation(IntMap)
  

module Canonical = MakeCanonical(Int) 

