open PqTreeLib
open QCheck2


module Print : 
sig 
  val print : Format.formatter -> int PqTree.t -> unit 
end 

val to_string : int PqTree.t -> string 


module Enum : 
sig 
  include FeatCore.EnumSig.ENUM
  val enumeration : int PqTree.t enum
end


module IntMap : Map.S with type key = int 

module CheckPermutation : PqTree.CheckPermutationSig with type elt = int 

module Canonical : PqTree.CanonicalSig with type elt = int 


val sized_pqtree_gen : int -> int PqTree.t Gen.t 

val pqtree_gen : int PqTree.t Gen.t 

val block_list_gen : int -> int -> (int PqTree.t * int list list) Gen.t


val permutation_gen : 'elt PqTree.t -> 'elt list Gen.t 

val interval_gen : 'elt PqTree.t -> 'elt list Gen.t 



val sized_pqtree_interval_gen : int -> (int PqTree.t * int list) Gen.t 


val sized_shuffled_pair_gen : int -> (int PqTree.t * int PqTree.t) Gen.t 

