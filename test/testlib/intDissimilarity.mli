open DissimilarityLib
open QCheck2

val shrink : 'elt Dissimilarity.t -> 'elt Dissimilarity.t Seq.t

val primitive_gen : (int -> 'elt Dissimilarity.t) -> int -> 'elt Dissimilarity.t Gen.t


val toeplitz : k:int -> int -> int Dissimilarity.t Gen.t 

val uniform : int -> int Dissimilarity.t Gen.t 

val from_intervals : k:int -> int -> int Dissimilarity.t Gen.t 

val from_rectangles : width:float -> height:float -> int -> int Dissimilarity.t Gen.t

val to_string : 'elt Dissimilarity.t -> string 
