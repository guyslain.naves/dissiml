let out_channel = open_out "prof.json"

let () = 
  let open Landmark in 
  let options = Landmark.profiling_options ()  in 
  Landmark.set_profiling_options
    { options with 
      format = JSON;
      output = Channel out_channel 
    }

open PqTreeLib

module IntMap = Map.Make(Int)
module Checker = PqTree.MakeCheckPermutation(IntMap)

let () = 
  let pqtree = PqTree.sample 60 in 
  let checker = Checker.checker pqtree in
  for _i = 0 to 99 do 
    let permutation = PqTree.sample_permutation pqtree in 
    let[@landmark] res = Checker.checks checker permutation in
    if not (res) then 
      begin 
        Format.fprintf Format.err_formatter "error in permutation checker";
        exit 1;
      end 
  done
