
open PqTreeLib
open RobinsonTest 

open IntPqTree 

let is_in interval i = List.mem i interval 

let insert interval tree = 
  FunctionalBL.add_interval (is_in interval) (List.length interval) tree

let test_naive (tree, interval, expected) = 
  Option.compare Canonical.compare 
    (insert interval tree)
    expected 
  = 0

let%test _ = test_naive
  ( P [Q [Leaf 1; Leaf 2; Leaf 3]; Leaf 4; Leaf 5],
    [3;4],
    Some (P [Q [Leaf 1; Leaf 2; Leaf 3; Leaf 4]; Leaf 5])
  )


let%test _ = test_naive
  ( P [Leaf 1; Leaf 2; Leaf 3; Leaf 4; Leaf 5],
    [1;3;5],
    Some (P [P [Leaf 1; Leaf 3; Leaf 5]; Leaf 2; Leaf 4])
  )


let%test _ = test_naive
  ( P [ Leaf 1; P [ Leaf 2; Leaf 3; Leaf 4; Leaf 5]],
    [1;2;5],
    Some (Q [P [Leaf 3; Leaf 4]; P [Leaf 2; Leaf 5]; Leaf 1])
  )


let%test _ = test_naive
  (P [ Leaf 1; Q [Leaf 2; Leaf 3]; Leaf 4; Q [Leaf 5; Leaf 6]; Leaf 7],
   [2;4;6],
   Some (P [Q [Leaf 3; Leaf 2; Leaf 4; Leaf 6; Leaf 5]; Leaf 1; Leaf 7])
  )


let%expect_test _ = 
  insert [2;4;6] (Q [ Leaf 1; Q [Leaf 2; Leaf 3]; Leaf 4; Q [Leaf 5; Leaf 6]; Leaf 7])
  |> Option.map Canonical.canonical
  |> Option.map to_string
  |> Option.value ~default:"not an interval"
  |> print_endline;
  [%expect{|Q[Leaf 1;Leaf 3;Leaf 2;Leaf 4;Leaf 6;Leaf 5;Leaf 7]|}]


let%expect_test _ = 
  insert [2;3;7] (P [ Leaf 1; Q [Leaf 2; Leaf 3]; Leaf 4; Q [Leaf 5; Leaf 6]; Leaf 7])
  |> Option.map Canonical.canonical
  |> Option.map to_string
  |> Option.value ~default:"not an interval"
  |> print_endline;
  [%expect{|P[Leaf 1;P[Q[Leaf 2;Leaf 3];Leaf 7];Leaf 4;Q[Leaf 5;Leaf 6]]|}]

let%expect_test _ = 
  insert [1;2;3;4;6] (Q [ Leaf 1; Q [Leaf 2; Leaf 3]; Leaf 4; Q [Leaf 5; Leaf 6]; Leaf 7])
  |> Option.map Canonical.canonical
  |> Option.map to_string
  |> Option.value ~default:"not an interval"
  |> print_endline;
  [%expect{|Q[Leaf 1;Q[Leaf 2;Leaf 3];Leaf 4;Leaf 6;Leaf 5;Leaf 7]|}]

let%expect_test _ = 
  insert [2;4] (Q [ Leaf 1; P [Leaf 2; Leaf 3; Leaf 4; Leaf 5]; Leaf 6; Q [Leaf 7; Leaf 8]; Leaf 9])
  |> Option.map Canonical.canonical
  |> Option.map to_string
  |> Option.value ~default:"not an interval"
  |> print_endline;
  [%expect{|Q[Leaf 1;P[P[Leaf 2;Leaf 4];Leaf 3;Leaf 5];Leaf 6;Q[Leaf 7;Leaf 8];Leaf 9]|}]


let%expect_test _ = 
  insert [2;4] (Q [ Leaf 1; Q [Leaf 2; Leaf 3; Leaf 4; Leaf 5]; Leaf 6; Q [Leaf 7; Leaf 8]; Leaf 9])
  |> Option.map Canonical.canonical
  |> Option.map to_string
  |> Option.value ~default:"not an interval"
  |> print_endline;
  [%expect{|not an interval|}]


let%expect_test _ = 
  insert [1;5] (P [P [Leaf 1;Leaf 2]; P [Leaf 3;Leaf 4;Leaf 5]])
  |> Option.map Canonical.canonical
  |> Option.map to_string
  |> Option.value ~default:"not an interval"
  |> print_endline;
  [%expect{| Q[Leaf 2;Leaf 1;Leaf 5;P[Leaf 3;Leaf 4]] |}]


let%expect_test _ = 
  insert [5;6;8] (Q [Leaf 2; Leaf 1; Q [Leaf 3; Leaf 4; Leaf 5; Leaf 6]; P [Leaf 7; Leaf 8]; Leaf 9; Leaf 10; Leaf 11])
  |> Option.map Canonical.canonical
  |> Option.map to_string
  |> Option.value ~default:"not an interval"
  |> print_endline;
  [%expect{|
    Q[Leaf 2;Leaf 1;Leaf 3;Leaf 4;Leaf 5;Leaf 6;Leaf 8;Leaf 7;Leaf 9;Leaf 10;
        Leaf 11] |}]

