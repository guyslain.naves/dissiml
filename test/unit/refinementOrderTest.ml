open PqTreeLib
open PqTree 
open RobinsonTest 

open IntPqTree 

module IntSet = Set.Make(Int)


let pp_sep sep fmt () = Format.(fprintf fmt "%s@ " sep)

let pp_delimiter left right inside_formatter fmt value =
  Format.(fprintf fmt "%s%a%s" left inside_formatter value right)

let pp_delimited_seq left sep right elt_formatter = 
  let open Format in 
  pp_delimiter left right 
    (pp_print_seq elt_formatter ~pp_sep:(pp_sep sep))
 
let pp_block fmt block = 
  pp_delimited_seq "{" ";" "}" Format.pp_print_int fmt (IntSet.to_seq block)


let pp_block_seq =
  pp_delimited_seq "[" ";" "]" pp_block


let output_blocks blocks =
  Format.(printf "%a" pp_block_seq blocks) 
  

let output result = 
  result
  |> Option.map Canonical.canonical
  |> Option.map to_string
  |> Option.value ~default:"not an interval"
  |> print_endline

module M = PqRefinementOrder.Make(IntSet)



let tree0 = P [Leaf 1; Leaf 2; Leaf 3]

let%expect_test _ = 
  M.enumerate_non_trivial_blocks tree0
  |> output_blocks; [%expect{|
    []|}]


let tree1 = Q [Leaf 1; Leaf 2; Leaf 3]

let%expect_test _ = 
  M.enumerate_non_trivial_blocks tree1
  |> output_blocks; [%expect{|
    [{2; 3}; {1;
    2}]|}]


let tree2 = P [ Q [Leaf 1; Leaf 2; Leaf 3]; P [Leaf 4; Leaf 5]] 

let%expect_test _ = 
  M.enumerate_non_trivial_blocks tree2
  |> output_blocks; [%expect{|
    [{1; 2; 3}; {4; 5}; {2; 3}; {1;
    2}]|}]

let tree3 = 
  Q [
    P [Leaf 1; Leaf 2; Leaf 3]; 
    Q [Leaf 4; Leaf 5; Leaf 6]; 
    Leaf 7;
    P [P [Leaf 8; Leaf 9]; Leaf 10; Leaf 11]
  ]

let%expect_test _ = 
  M.enumerate_non_trivial_blocks tree3
  |> output_blocks; [%expect{|
    [{4; 5; 6; 7; 8; 9; 10; 11}; {7; 8; 9; 10; 11}; {1; 2; 3; 4; 5; 6}; {1; 2; 3;
    4; 5; 6; 7}; {1; 2; 3}; {4; 5; 6}; {8; 9; 10; 11}; {5; 6}; {4; 5}; {8;
    9}]|}]

let%expect_test _ =
  [ ([1;2], tree1);
    ([2;3], tree1);
    ([4;5], tree2);
    ([1;2;3;4;5], tree2);
    ([1;2;3], tree2);
    ([2], tree2);
    ([4;5;6;7], tree3);
    ([1;2;3;4;5;6], tree3);
    ([8;9],tree3);
    ([1;3], tree0);
    ([1;3], tree1);
    ([1;3], tree2);
    ([1;2;4;5], tree2);
    ([2;3;4;5;6], tree3);
    ([1;2;3;7], tree3);
    ([8;9;10], tree3);
  ]
  |> List.to_seq
  |> Seq.map (fun (block, tree) -> M.is_a_block tree (IntSet.of_list block)) 
  |> Format.printf "%a" (pp_delimited_seq "[" ";" "]" Format.pp_print_bool);
  [%expect{|
    [true; true; true; true; true; true; true; true; true; false; false; false;
    false; false; false;
    false] |}]
