open PqTreeLib
open PqTree 
open PqChainInsertion 
open RobinsonTest 

open IntPqTree 


let output result = 
  result
  |> Option.map Canonical.canonical
  |> Option.map to_string
  |> Option.value ~default:"not an interval"
  |> print_endline



let%expect_test _ = 
  refine_by_distances 0 (fun n -> n/2) (P [Leaf 6; Leaf 3])
  |> output; [%expect{|P[Leaf 3;Leaf 6]|}]



let tree1 = P [Leaf 5; Leaf 2; Leaf 0; Leaf 1; Leaf 3; Leaf 1; Leaf 4]

let%expect_test _ = 
  refine_by_distances 0 (fun n -> n) tree1
  |> output; [%expect{|P[P[P[P[P[Leaf 0;Leaf 1;Leaf 1];Leaf 2];Leaf 3];Leaf 4];Leaf 5]|}]
    

let %expect_test _ = 
  refine_by_distances 0 (fun n -> n/2) tree1 
  |> output; [%expect{|P[P[P[Leaf 0;Leaf 1;Leaf 1];Leaf 2;Leaf 3];Leaf 4;Leaf 5]|}]



let tree2 = P [ P [Leaf 5; Leaf 2]; P [Leaf 0; Leaf 1]; P [Leaf 6; Leaf 3]; Leaf 4]

let%expect_test _ = 
  refine_by_distances 0 (fun n -> n) tree2
  |> output; [%expect{|not an interval|}]

let%expect_test _ = 
  refine_by_distances 0 (fun n -> n/2) tree2
  |> output; [%expect{|Q[Leaf 4;Leaf 5;Leaf 2;P[Leaf 0;Leaf 1];Leaf 3;Leaf 6]|}]



let tree3 = Q [ P [ Leaf 1; Leaf 2; Leaf 3]; P [Leaf 1; Leaf 0; Leaf 1]; P [ Leaf 1; Leaf 2; Leaf 3] ]

let%expect_test _ = 
  refine_by_distances 0 (fun n -> n) tree3
  |> output; [%expect{|Q[Leaf 3;Leaf 2;Leaf 1;P[Leaf 0;Leaf 1;Leaf 1];Leaf 1;Leaf 2;Leaf 3]|}]

let%expect_test _ = 
  refine_by_distances 0 (fun n -> n/2) tree3
  |> output; [%expect{|Q[P[Leaf 2;Leaf 3];Leaf 1;P[Leaf 0;Leaf 1;Leaf 1];Leaf 1;P[Leaf 2;Leaf 3]]|}]



let tree4 = Q [ Q [Leaf 3; Leaf 6; Leaf 9]; Leaf 0; Q [ Leaf 3; Leaf 4; Leaf 5 ]; Q [ Leaf 6; Leaf 8; Leaf 9]]

let%expect_test _ = 
  refine_by_distances 0 (fun n -> n/3) tree4
  |> output; [%expect{|Q[Leaf 9;Leaf 6;Leaf 3;Leaf 0;Q[Leaf 3;Leaf 4;Leaf 5];Leaf 6;Leaf 8;Leaf 9]|}]

let tree5 = Q [ Q [Leaf 3; Leaf 6; Leaf 9]; Leaf 0; Q [ Leaf 3; Leaf 4; Leaf 5 ]; P [ Leaf 6; Leaf 8; Leaf 9]]

let%expect_test _ = 
  refine_by_distances 0 (fun n -> n/3) tree5
  |> output; [%expect{|Q[Leaf 9;Leaf 6;Leaf 3;Leaf 0;Q[Leaf 3;Leaf 4;Leaf 5];P[Leaf 6;Leaf 8];Leaf 9]|}]


let tree_0 = P [ Leaf 0; Leaf 1; Leaf 2; Leaf 3; Leaf 4; Leaf 5; Leaf 6; Leaf 7; Leaf 8; Leaf 9 ]
let d0 = [|0;63;11;66;52;12;69;16;11;9|]

let%expect_test _ = 
  refine_by_distances 0 (fun n -> d0.(n)) tree_0
  |> output; [%expect{|
    P[P[P[P[P[P[P[P[Leaf 0;Leaf 9];Leaf 2;Leaf 8];Leaf 5];Leaf 7];Leaf 4];Leaf 1];
          Leaf 3];Leaf 6]|}]

let tree_1 = P[P[P[P[P[P[P[P[Leaf 0;Leaf 9];Leaf 2;Leaf 8];Leaf 5];Leaf 7];Leaf 4];Leaf 1];Leaf 3];Leaf 6]
let d1 = [|63;0;53;5;12;74;8;78;52;57|]

let%expect_test _ = 
  refine_by_distances 1 (fun n -> d1.(n)) tree_1
  |> output; [%expect{|Q[Leaf 6;Leaf 3;Leaf 1;Leaf 4;Leaf 8;Leaf 2;Leaf 9;Leaf 0;Leaf 5;Leaf 7]|}]

let tree_2 = Q[Leaf 6;Leaf 3;Leaf 1;Leaf 4;Leaf 8;Leaf 2;Leaf 9;Leaf 0;Leaf 5;Leaf 7]
let d2 = [|11;53;0;56;41;22;59;27;8;5|]

let%expect_test _ = 
  refine_by_distances 2 (fun n -> d2.(n)) tree_2
  |> output; [%expect{|Q[Leaf 6;Leaf 3;Leaf 1;Leaf 4;Leaf 8;Leaf 2;Leaf 9;Leaf 0;Leaf 5;Leaf 7]|}]

let d3 = [|66;5;56;0;15;77;3;82;56;60|]
let d4 = [|52;12;41;15;0;63;18;67;42;46|]

let%expect_test _ = 
  refine_by_distances 3 (fun n -> d3.(n)) tree_2
  |> output; [%expect{|Q[Leaf 6;Leaf 3;Leaf 1;Leaf 4;Leaf 8;Leaf 2;Leaf 9;Leaf 0;Leaf 5;Leaf 7]|}]
let%expect_test _ = 
  refine_by_distances 4 (fun n -> d4.(n)) tree_2
  |> output; [%expect{|not an interval|}]



let interval_distance elements i = 
  if List.mem i elements then 0
  else 1

let ptree1 = 
  P[Leaf 0;Leaf 1;
    P[Leaf 2;Leaf 3;Leaf 4;
      P[P[P[Leaf 5;Leaf 6];Leaf 7];
        Leaf 8;
        P[Leaf 9;Leaf 10];
        Leaf 11];
      P[Leaf 12;Leaf 13]];
    Leaf 14;Leaf 15;Leaf 16;Leaf 17;
    P[P[Leaf 18;Leaf 19];Leaf 20;Leaf 21];
    Leaf 22
   ]

let%expect_test _ = 
  refine_by_distances 10 (interval_distance [10;12]) ptree1 
  |> output; [%expect{|
    P[Leaf 0;Leaf 1;
        P[Leaf 2;Leaf 3;Leaf 4;
            Q[P[P[P[Leaf 5;Leaf 6];Leaf 7];Leaf 8;Leaf 11];Leaf 9;Leaf 10;
                Leaf 12;Leaf 13]];Leaf 14;Leaf 15;Leaf 16;Leaf 17;
        P[P[Leaf 18;Leaf 19];Leaf 20;Leaf 21];Leaf 22] |}]


let ptree2 = 
  P[Leaf 2;Leaf 3;Leaf 4;
    P[P[P[Leaf 5;Leaf 6];Leaf 7];Leaf 8;P[Leaf 9;Leaf 10];Leaf 11];
    P[Leaf 12;Leaf 13]]

let%expect_test _ = 
  refine_by_distances 10 (interval_distance [10;12]) ptree2
  |> output; [%expect{|
    P[Leaf 2;Leaf 3;Leaf 4;
        Q[P[P[P[Leaf 5;Leaf 6];Leaf 7];Leaf 8;Leaf 11];Leaf 9;Leaf 10;Leaf 12;
            Leaf 13]] |}]


let ptree3 = 
  P[Leaf 2;
    P[Leaf 7;P[Leaf 9;Leaf 10]];
    P[Leaf 12;Leaf 13]
   ]

let%expect_test _ = 
  refine_by_distances 10 (interval_distance [10;12]) ptree3
  |> output; [%expect{| P[Leaf 2;Q[Leaf 7;Leaf 9;Leaf 10;Leaf 12;Leaf 13]] |}]

let ptree4 = 
    P[Leaf 7;P[Leaf 8; Leaf 9;Leaf 10];Leaf 11]

let%expect_test _ = 
  refine_by_distances 10 (interval_distance [8;10]) ptree4
  |> output; [%expect{| P[Leaf 7;P[P[Leaf 8;Leaf 10];Leaf 9];Leaf 11] |}]


let ptree5 = 
  P[
    Leaf 4;Leaf 23;Leaf 25;Leaf 29;Leaf 33;Leaf 35;Leaf 38;
    Q[
      P[Leaf 15;Leaf 0;Leaf 13;Leaf 37;Leaf 9;Leaf 22;Leaf 11;Leaf 16;
        Leaf 7;Leaf 24;Leaf 20;Leaf 30;Leaf 21;Leaf 6;Leaf 27;Leaf 39;
        Leaf 28;
        P[Leaf 26;Leaf 17;Leaf 34;Leaf 5];
        P[Leaf 10;Leaf 3]
       ];
      P[Leaf 14;Leaf 19;Leaf 1;Leaf 8;Leaf 2;Leaf 31];
      P[Leaf 12;Leaf 32;Leaf 36;Leaf 18]
    ]
  ]

let%expect_test _ = 
  refine_by_distances 0 (interval_distance [0; 20; 37; 15; 14; 19; 31; 1; 2; 8; 12; 36; 32; 18; 29]) ptree5
    |> output; [%expect{|
      P[Q[P[P[Leaf 3;Leaf 10];P[Leaf 5;Leaf 17;Leaf 26;Leaf 34];Leaf 6;Leaf 7;
              Leaf 9;Leaf 11;Leaf 13;Leaf 16;Leaf 21;Leaf 22;Leaf 24;Leaf 27;
              Leaf 28;Leaf 30;Leaf 39];P[Leaf 0;Leaf 15;Leaf 20;Leaf 37];
            P[Leaf 1;Leaf 2;Leaf 8;Leaf 14;Leaf 19;Leaf 31];
            P[Leaf 12;Leaf 18;Leaf 32;Leaf 36];Leaf 29];Leaf 4;Leaf 23;Leaf 25;
          Leaf 33;Leaf 35;Leaf 38] |}]


let ptree6 = 
  P[
    Leaf 1;Leaf 2;Leaf 3;
    Q[
      P[Leaf 4;Leaf 5;Leaf 6;Leaf 7];
      P[Leaf 8;Leaf 9];
      P[Leaf 10;Leaf 11]
    ]
  ]

let%expect_test _ = 
  refine_by_distances 6 (interval_distance [2; 4; 6; 8; 9; 10; 11]) ptree6
    |> output; [%expect{|
      P[Leaf 1;
          Q[Leaf 2;P[Leaf 10;Leaf 11];P[Leaf 8;Leaf 9];P[Leaf 4;Leaf 6];
              P[Leaf 5;Leaf 7]];Leaf 3] |}]
