open PqTreeLib
open PqTree 

let run pqtree interval = 
  let algos = ImperativeBL.get_algorithms pqtree (fun i -> i-1) in 
  let _ = algos.insert interval in
  algos.get ()
  |> RobinsonTest.IntPqTree.to_string 
  |> print_endline


let pqtree1 =
  P
    [Q
       [Leaf 1; Leaf 2;
        P
          [P
             [Leaf 3;
              P
                [Leaf 4;
                 P
                   [Leaf 5;
                    Q
                      [Leaf 6;
                       P
                         [Q
                            [Leaf 7; Q [Leaf 8; Leaf 9; Leaf 10; Leaf 11; Leaf 12];
                             Leaf 13; Leaf 14; Leaf 15; Leaf 16; Leaf 17; Leaf 18;
                             Leaf 19; Leaf 20;
                             P
                               [Leaf 21; Leaf 22; Leaf 23; Leaf 24; Leaf 25; Leaf 26;
                                Leaf 27; Leaf 28; Leaf 29]];
                          Leaf 30; Q [Leaf 31; Leaf 32; Leaf 33]];
                       Leaf 34];
                    P [Leaf 35; P [Leaf 36; Leaf 37; Leaf 38]; Leaf 39]]]];
           Leaf 40];
        Leaf 41];
     P [Leaf 42; Leaf 43]]
let interval1 = [8;9;10;11;12;13] 


let%expect_test _ = 
  run pqtree1 interval1;
  [%expect{|
    P[Q[Leaf 1;Leaf 2;
          P[P[Leaf 3;
                P[Leaf 4;
                    P[Leaf 5;
                        Q[Leaf 6;
                            P[Q[Leaf 7;Q[Leaf 8;Leaf 9;Leaf 10;Leaf 11;Leaf 12];
                                  Leaf 13;Leaf 14;Leaf 15;Leaf 16;Leaf 17;
                                  Leaf 18;Leaf 19;Leaf 20;
                                  P[Leaf 21;Leaf 22;Leaf 23;Leaf 24;Leaf 25;
                                      Leaf 26;Leaf 27;Leaf 28;Leaf 29]];Leaf 30;
                                Q[Leaf 31;Leaf 32;Leaf 33]];Leaf 34];
                        P[Leaf 35;P[Leaf 36;Leaf 37;Leaf 38];Leaf 39]]]];
              Leaf 40];Leaf 41];P[Leaf 42;Leaf 43]] |}]


let pqtree2 = 
  Q [
      Leaf 1;
      P [
          Q [ 
              P [ 
                  Leaf 2;
                  P [ 
                      Leaf 3; 
                      P [Leaf 4;Leaf 5]; 
                      P[ P [Leaf 6;Leaf 7]; Leaf 8]
                    ];
                  P [Leaf 9; Leaf 10];
                  Leaf 11
                ];
              P [ 
                  Leaf 12;
                  Q [Leaf 13; Leaf 14; Leaf 15]
                ];
              Leaf 16
            ];
          Leaf 17;
          Leaf 18
        ];
      Leaf 19
    ]
let interval2 = [8; 7; 6; 5; 4; 3; 2]

let%expect_test _ = 
  run pqtree2 interval2;
  [%expect{|
    Q[Leaf 1;
        P[Q[P[P[Leaf 9;Leaf 10];Leaf 11;
                P[P[Leaf 3;P[Leaf 4;Leaf 5];P[P[Leaf 6;Leaf 7];Leaf 8]];Leaf 2]];
              P[Leaf 12;Q[Leaf 13;Leaf 14;Leaf 15]];Leaf 16];Leaf 17;Leaf 18];
        Leaf 19] |}]





let speed_test n =
  let open DataStruct in 
  let elts = MoreList.range 0 (n-1) in 
  let pairs = 
    elts 
    |> MoreList.consecutives
    |> Combi.Combinatorics.random_permutation
  in 
  let leaves = List.map (fun i -> Leaf i) elts in
  let universal = P leaves in 
  let flat = Q leaves in 
  let algos = ImperativeBL.get_algorithms universal (fun i -> i) in 
  List.iter (fun (i,j) -> assert (algos.insert [i;j])) pairs;
  RobinsonTest.IntPqTree.Canonical.compare flat (algos.get ()) = 0



let%test _ = 
  speed_test 100000

        
                     
  
