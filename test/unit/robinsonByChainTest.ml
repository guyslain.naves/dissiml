open RobinsonTest.IntPqTree

open ChainLib


let output result = 
  result
  |> Option.map Canonical.canonical
  |> Option.map to_string
  |> Option.value ~default:"not an interval"
  |> print_endline


let print_order order = 
  let open Format in 
  fprintf std_formatter
    "%a\n" 
    (pp_print_list ~pp_sep:(fun fmt () -> pp_print_string fmt ", ") pp_print_int)
    order


let diss1 = RandomDissimilarity.Toeplitz.toeplitz012 ~k:1 ~n:10 

let%expect_test _ = 
  begin match RobinsonByChain.find_compatible_order diss1 with 
  | Some order -> print_order order 
  | None -> Format.(fprintf std_formatter "not Robinson")
  end;
  [%expect{| 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 |}]
  

let diss2 = RandomDissimilarity.Toeplitz.toeplitz012 ~k:3 ~n:20

let%expect_test _ = 
  begin match RobinsonByChain.find_compatible_order diss2 with 
  | Some order -> print_order order 
  | None -> Format.(fprintf std_formatter "not Robinson")
  end;
  [%expect{| 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 |}]
