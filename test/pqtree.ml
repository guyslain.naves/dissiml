open RobinsonLib
open PqTree 

let is_in interval i = List.mem i interval 

let test_naive tree interval = 
  Naive_pqtree.add_interval (is_in interval) (List.length interval) tree

let test1 = 
  ( P [Q [Leaf 1; Leaf 2; Leaf 3]; Leaf 4; Leaf 5],
    [3;4],
    P [Q [Leaf 1; Leaf 2; Leaf 3; Leaf 4]; Leaf 5]
  )



let test2 =
  ( P [Leaf 1; Leaf 2; Leaf 3; Leaf 4; Leaf 5],
    [1;3;5],
    P [P [Leaf 1; Leaf 3; Leaf 5]; Leaf 2; Leaf 4]
  )


let test3 = 
  ( P [ Leaf 1; P [ Leaf 2; Leaf 3; Leaf 4; Leaf 5]],
    [1;2;5],
    Q [P [Leaf 3; Leaf 4]; P [Leaf 2; Leaf 5]; Leaf 1]
  )

