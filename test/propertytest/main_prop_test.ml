let all_tests = 
  List.flatten 
    [ Pqtree.tests;
      FunctionalBLtest.tests; 
      ImperativeBLTest.tests;
      Pivotpair.tests;
      ChainInsertion.tests
    ]

let%test _ =
  let errcode = 
    QCheck_base_runner.run_tests 
      ~verbose:true 
      all_tests 
  in
  errcode = 0
