open Recognition 
open DissimilarityLib.Dissimilarity
open QCheck2 
open RobinsonTest 

let rev_index { elements; _ } elt =
  let indexed = 
    elements 
    |> List.mapi (fun j e -> (e,j)) 
  in 
  List.assoc elt indexed

let pivotpair_algorithm_terminates diss = 
  match Pivotpair.algo diss (rev_index diss) with 
  | Some order ->
     DissimilarityLib.Dissimilarity.is_compatible_order diss order 
  | None -> true
   

let test_pivotpair_algorithm_terminates =
  Test.make 
    ~name:"Algorithm pivotpair terminates without error"
    ~count:1000
    ~print:IntDissimilarity.to_string
    (IntDissimilarity.from_rectangles ~width:100. ~height:10. 10)
    (* Gen.(
     *   int_range 1 20 >>= fun k -> 
     *   int_range 2 100 >>= fun n -> 
     *   IntDissimilarity.from_intervals ~k n >>= fun diss -> 
     *   return diss
     * ) *)
  pivotpair_algorithm_terminates






let blocks_discriminates_triplets diss =
  let d = diss.d in 
  (d 0 2 < max (d 0 1) (d 1 2))
  || ( match Pivotpair.find_blocks diss 0 2 with 
       | Some blocks -> 
          List.exists (fun b -> List.mem 0 b && List.mem 2 b && not (List.mem 1 b)) blocks
       | None -> true
     )

let test_blocks_discriminates_triplets = 
  Test.make
    ~name:"Blocks used by pivoting discriminates invalid triplets"
    ~count:1000
    ~print:IntDissimilarity.to_string 
    (IntDissimilarity.from_rectangles ~width:100. ~height:10. 20)
    blocks_discriminates_triplets

let tests = 
  [
    test_pivotpair_algorithm_terminates;
    (* test_blocks_discriminates_triplets; *)
  ]
