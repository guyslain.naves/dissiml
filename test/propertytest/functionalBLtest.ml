open PqTreeLib
open DataStruct
open RobinsonTest
open IntPqTree 

open QCheck2


module IntSet = Set.Make(Int) 


let insert ints pqtree = 
  FunctionalBL.add_interval 
    (fun i -> IntSet.mem i ints) 
    (IntSet.cardinal ints) 
    pqtree  


let rec is_an_interval ints = function 
  | [] -> IntSet.cardinal ints = 0
  | (hd :: _) as permutation when IntSet.mem hd ints -> 
     let length = IntSet.cardinal ints in 
     (permutation |> MoreList.take length |> List.for_all (fun i -> IntSet.mem i ints))
     && (permutation |> MoreList.drop length |> List.for_all (fun i -> not (IntSet.mem i ints)))
  | _ :: permutation -> is_an_interval ints permutation




let interval_insertion_does_not_fail (pqtree,interval) =
  insert (IntSet.of_list interval) pqtree 
  |> Option.is_some

let test_interval_insertion_does_not_fail =
  Test.make 
    ~name:"An interval of a PQ-tree may be inserted in that PQ-tree"
    ~print:Print.(pair to_string (list int))
    Gen.(int_range 1 100 >>= sized_pqtree_interval_gen)
    interval_insertion_does_not_fail


let inserted_interval_is_interval_of_sampled_permutation (pqtree, interval) =
  let interval = IntSet.of_list interval in
  match insert interval pqtree with 
  | None -> true
  | Some pqtree -> 
     MoreList.range 0 99 
     |> List.for_all (fun _ ->  
            let permutation = PqTree.sample_permutation pqtree in 
            is_an_interval interval permutation 
          )

let test_inserted_interval_is_interval_of_sampled_permutation =
  Test.make 
    ~name:"An interval inserted in a PQ-tree is always an interval of any sampled permutation"
    ~print:Print.(pair to_string (list int))
    Gen.(int_range 1 100 >>= sized_pqtree_interval_gen)
    inserted_interval_is_interval_of_sampled_permutation


let insert_is_idempotent (pqtree,interval) =
  let interval = IntSet.of_list interval in 
  let pqtree' = insert interval pqtree in 
  let pqtree'' = Option.bind pqtree' (insert interval) in 
  Option.compare IntPqTree.Canonical.compare pqtree' pqtree'' = 0

let test_insert_is_idempotent = 
  Test.make 
  ~name:"insert is idempotent" 
  ~print:Print.(pair to_string (list int)) 
  Gen.(int_range 1 100 >>= sized_pqtree_interval_gen)
  insert_is_idempotent 

let tests = 
  [ 
    test_interval_insertion_does_not_fail;
    test_inserted_interval_is_interval_of_sampled_permutation;
    test_insert_is_idempotent;
  ]
