open PqTreeLib
open RobinsonTest
open PqTree
open IntPqTree 

open QCheck2

let print = IntPqTree.to_string


let rec arity_checks = function 
| Leaf _ -> true
| P children -> List.length children >= 2 && List.for_all arity_checks children 
| Q children -> List.length children >= 3 && List.for_all arity_checks children

let test_arities_of_sampled_pqtree_are_valid = 
  QCheck2.Test.make
    ~name:"In sampled pqtrees, P nodes have >= 2 children, Q nodes >= 3 children"
    ~count:100
    ~print
    Gen.(int_range 1 100 >>= sized_pqtree_gen)
    arity_checks


let test_shrinked_pq_trees_have_valid_arities = 
  QCheck2.Test.make 
    ~name:"Shrinked pqtrees have valid degrees"
    ~print
    pqtree_gen 
    (fun tree -> Seq.fold_left (&&) true (Seq.map arity_checks (shrink tree)))


let frontier_is_a_contained_permutation pqtree =
  CheckPermutation.contains_permutation pqtree (frontier pqtree)

let test_frontier_is_a_contained_permutation = 
  QCheck2.Test.make
    ~name:"The frontier of a PQ-tree is contained in that PQ-tree"
    ~print 
    pqtree_gen 
    frontier_is_a_contained_permutation 




let sized_pq_tree_permutation_gen size = 
  let open Gen in 
  sized_pqtree_gen size >>= fun pqtree -> 
  permutation_gen pqtree >>= fun permutation ->
  return (pqtree, permutation)
  

let sampled_permutation_is_contained_in_pqtree (pqtree, permutation) = 
  CheckPermutation.contains_permutation pqtree permutation 

let test_sampled_permutation_is_contained_in_pqtree = 
  QCheck2.Test.make 
    ~name:"A sampled permutation from a PQ-tree is contained in that PQ-tree"
    ~print:QCheck2.Print.(pair print (list int))
    Gen.(int_range 1 100 >>= sized_pq_tree_permutation_gen) 
    sampled_permutation_is_contained_in_pqtree



let shuffled_is_equal_to_pqtree (tpeqre, pqtree) =
  Canonical.compare tpeqre pqtree = 0 

let test_shuffled_is_equal_to_pqtree =
  QCheck2.Test.make 
    ~name:"a PQ-tree and its shuffle must be equal"
    ~print:QCheck2.Print.(pair print print) 
    Gen.(int_range 1 100 >>= sized_shuffled_pair_gen)
  shuffled_is_equal_to_pqtree
  


let shuffled_frontier_is_a_contained_permutation (tpeqre,pqtree) =
  CheckPermutation.contains_permutation pqtree (frontier tpeqre) 

let test_shuffled_frontier_is_a_contained_permutation = 
  QCheck2.Test.make 
    ~name:"The frontier of a shuffled PQ-tree must be a valid permutation"
    ~print:QCheck2.Print.(pair print print)
    Gen.(int_range 1 100 >>= sized_shuffled_pair_gen)
    shuffled_frontier_is_a_contained_permutation


let sampled_compatible_order_is_a_valid_permutation (permutation, pqtree) =
  CheckPermutation.contains_permutation pqtree permutation  

let test_sampled_compatible_order_is_a_valid_permutation = 
  QCheck2.Test.make 
    ~name:"Sampled compatible order are valid permutations"
    ~print:QCheck2.Print.(pair (list int) print)
    ~count:100
    Gen.(pqtree_gen >>= fun pqtree -> return (PqTree.sample_compatible_order pqtree, pqtree))
    sampled_compatible_order_is_a_valid_permutation



let tests = 
  [ test_arities_of_sampled_pqtree_are_valid;
    test_shrinked_pq_trees_have_valid_arities;
    test_frontier_is_a_contained_permutation;
    test_sampled_permutation_is_contained_in_pqtree;
    test_shuffled_is_equal_to_pqtree;
    test_shuffled_frontier_is_a_contained_permutation;
    test_sampled_compatible_order_is_a_valid_permutation
  ]
