open PqTreeLib
open RobinsonTest
open IntPqTree
open QCheck2 

let make_algorithms pqtree =
  ImperativeBL.get_algorithms pqtree (fun i -> i)

let fun_insert interval pqtree =
  FunctionalBL.add_interval
    (fun i -> List.mem i interval) 
    (List.length interval) 
    pqtree 

let pqtree_to_impBLstruct_to_pqtree_is_identity pqtree =
  let algos = make_algorithms pqtree in 
  let pqtree' = algos.get () in 
  Canonical.compare pqtree pqtree' = 0


let test_pqtree_to_impBLstruct_to_pqtree_is_identity =
  Test.make 
    ~name:"PQ-tree to imperative BL struct to PQ-tree is identity"
    ~print:to_string 
    Gen.(int_range 2 100 >>= sized_pqtree_gen)
    pqtree_to_impBLstruct_to_pqtree_is_identity



let intervals_are_correctly_checked (pqtree, interval) =
  let algos = make_algorithms pqtree in 
  algos.is_interval interval 

let test_interval_are_correctly_checked =
  Test.make 
  ~name:"Intervals are correctly checked by Imperative BL"
  ~print:Print.(pair to_string (list int)) 
  Gen.(int_range 2 100 >>= sized_pqtree_interval_gen)
  intervals_are_correctly_checked
  


let inserted_interval_is_interval_in_resulting_pqtree (pqtree, interval, expected) =
  let algos = make_algorithms pqtree in 
  let is_success = algos.insert interval in 
  is_success 
  && Canonical.compare (algos.get ()) expected = 0
  


let test_inserted_interval_is_interval_in_resulting_pqtree = 
  Test.make 
    ~name:"An inserted interval is an interval in the resulting PQ-tree"
    ~count:1000
    ~print:Print.(triple to_string (list int) to_string)
    Gen.(int_range 2 100 >>= fun i->
         sized_pqtree_interval_gen i >>= fun (pqtree,interval) -> 
         return (pqtree, interval, Option.get (fun_insert interval pqtree))
    )
    inserted_interval_is_interval_in_resulting_pqtree


let resulting_tree_is_well_formed (pqtree, intervals) =
  let algos = make_algorithms pqtree in 
  let is_success = List.for_all algos.insert intervals in 
  is_success && (PqTree.is_well_formed (algos.get ()))


let test_resulting_tree_is_well_formed = 
  Test.make 
    ~name:"Tree resulting from inserting an interval is well-formed"
    ~count:1000
    ~print:Print.(pair to_string (list (list int))) 
    Gen.(
    int_range 1 10 >>= fun k ->
    int_range 2 100 >>= fun n -> 
    block_list_gen k n
  )
    resulting_tree_is_well_formed

let tests = 
  [ 
    test_pqtree_to_impBLstruct_to_pqtree_is_identity;
    test_interval_are_correctly_checked;
    test_inserted_interval_is_interval_in_resulting_pqtree;
    test_resulting_tree_is_well_formed;
  ]
    
