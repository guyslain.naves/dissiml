open PqTreeLib
open RobinsonTest 
open IntPqTree
open DataStruct

open QCheck2 


module IntSet = Set.Make(Int) 
module Ref = PqRefinementOrder.Make(IntSet)

let insert_interval interval pqtree = 
  let f i = if IntSet.mem i interval then 0 else 1 in 
  let s = IntSet.choose interval in
  PqChainInsertion.refine_by_distances s f pqtree 


let rec is_an_interval ints = function 
  | [] -> IntSet.cardinal ints = 0
  | (hd :: _) as permutation when IntSet.mem hd ints -> 
     let length = IntSet.cardinal ints in 
     (permutation |> MoreList.take length |> List.for_all (fun i -> IntSet.mem i ints))
     && (permutation |> MoreList.drop length |> List.for_all (fun i -> not (IntSet.mem i ints)))
  | _ :: permutation -> is_an_interval ints permutation


let interval_insertion_does_not_fail (pqtree,interval) =
  insert_interval (IntSet.of_list interval) pqtree 
  |> Option.is_some 


let test_interval_insertion_does_not_fail = 
  Test.make
    ~name:"Chain insertion generalizes interval insertion in PQ-tree"
    ~print:Print.(pair to_string (list int))
    ~count:1000
    Gen.(int_range 1 100 >>= sized_pqtree_interval_gen)
    interval_insertion_does_not_fail
    


let rec zip list1 list2 = match list1, list2 with 
| hd1::tl1, hd2::tl2 -> (hd1,hd2)::zip tl1 tl2
| _,_ -> [] 

  

let function_from_list assocs = 
  let map = 
    List.fold_left (fun map (arg,res) -> IntMap.add arg res map) IntMap.empty assocs 
  in 
  fun arg -> IntMap.find arg map

let print_tree_and_chain = 
  Print.(pair to_string (pair int (list (pair int int))))

let gen_tree_and_chain = 
  let open Gen in 
  list_size (return 100) bool >>= fun bools -> 
  int_range 1 50 >>=
  sized_shuffled_pair_gen >>= fun (shuffled, pqtree) ->
  let elts = PqTree.frontier shuffled in 
  let n = List.length elts in 
  int_range 0 (n-1) >>= fun pivot_rank -> 
  let ints =
    bools
    |> List.fold_left (fun (accu,i) bool -> (i::accu, if bool then i else i+1)) ([],0)
    |> fst
  in
  let pivot = List.nth elts pivot_rank in 
  let pivot_v = List.nth ints pivot_rank in 
  return (pqtree, (pivot, zip elts (List.map (fun i -> abs (i - pivot_v)) ints)))
  


let new_valid_permutation_is_old_valid_permutation (pqtree, (source,distances)) =
  let f = function_from_list distances in  
  match PqChainInsertion.refine_by_distances source f pqtree with
  | None -> false
  | Some new_pqtree -> Ref.is_refinement new_pqtree pqtree 

let test_new_valid_permutation_is_old_valid_permutation =
  Test.make
    ~name:"Chain insertion returns a refinement of the original PQ-tree"
    ~print:print_tree_and_chain
    ~count:100
    gen_tree_and_chain 
    new_valid_permutation_is_old_valid_permutation
    
  

let rec is_increasing = function 
| [] | [_] -> true
| x::y::tail -> x <= y && is_increasing (y::tail)

let rec is_bitonic = function 
| [] | [_] -> true
| x::y::tail when x >= y -> is_bitonic (y::tail)
| _::y::tail -> is_increasing (y::tail)



let permutations_are_bitonic (pqtree, (source, distances)) =
  let f = function_from_list distances in 
  match PqChainInsertion.refine_by_distances source f pqtree with 
  | None -> false
  | Some pqtree -> 
      Seq.repeat ()
      |> Seq.take 10 
      |> Seq.map (fun _ -> PqTree.sample_compatible_order pqtree)  
      |> Seq.map (List.map f)
      |> Seq.for_all is_bitonic

let test_permutations_are_bitonic = 
  Test.make 
    ~name:"Permutations of refined pqtree are bitonic relative to the distance to the source"
    ~print:print_tree_and_chain 
    ~count:1000
    gen_tree_and_chain 
    permutations_are_bitonic



let tests = 
  [ 
    test_interval_insertion_does_not_fail;
    test_new_valid_permutation_is_old_valid_permutation;
    test_permutations_are_bitonic
  ]
