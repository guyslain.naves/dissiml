open DissimilarityLib

let from_points_in_rectangle ~width ~height ~count =
  let random_point _ = (Random.float width, Random.float height) in 
  let dist (x1,y1) (x2,y2) = 
    hypot (x1 -. x2) (y1 -. y2) |> ceil |> Float.to_int 
  in 
  let points = Array.init count random_point in 
  let matrix = 
    Array.init count 
      (fun i -> 
        Array.init count 
          (fun j -> dist points.(i) points.(j))
      )
  in 
  let elements = DataStruct.MoreList.range 0 (count-1) in 
  let d i j = matrix.(i).(j) in 
  Dissimilarity.({ elements; d})
  
