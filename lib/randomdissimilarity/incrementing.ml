open DissimilarityLib 



let random prng_state n = 
  Random.set_state prng_state; 
  let elements = DataStruct.MoreList.range 0 (n-1) in 
  let array = Array.init n (fun _ -> Array.make n 0) in 
  for i = 0 to n-2 do 
    array.(i).(i+1) <- 1;
    array.(i+1).(i) <- 1;
  done;
  for k = 2 to n-1 do 
    for i = 0 to n-k-1 do
      let j = i + k in 
      array.(i).(j) <- 
        max array.(i+1).(j) array.(i).(j-1)
        + Random.int 2;
      array.(j).(i) <- array.(i).(j);
    done 
  done;
  let d i j = array.(i).(j) in 
  Dissimilarity.({ elements; d}) 
    
   
