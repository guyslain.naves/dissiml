open PqTreeLib
open DissimilarityLib 
open PqTree 



let from_pqtree pqtree =
  let n = PqTree.length pqtree in 
  let array = Array.init n (fun _ -> Array.make n (-1)) in 
  for i = 0 to n-1 do array.(i).(i) <- 0 done; 
  let set i j delta = 
    array.(i).(j) <- delta;
    array.(j).(i) <- delta;
  in 
  let (>>|) l f = List.iter f l in 
  let annotate_p delta children =
    DataStruct.MoreList.extend children >>| fun (child,_,post) ->
    child >>| fun i -> 
    post >>| fun child2 ->
    child2 >>| fun j -> 
    set i j delta 
  in 
  let annotate_q delta children =
    DataStruct.MoreList.extend children >>| fun (child,_,post) -> 
    child >>| fun i -> 
    match post with 
    | [] -> ();
    | next_child::others -> 
       (next_child >>| fun j -> set i j delta);
       others >>| fun child2 -> 
       child2 >>| fun j -> 
       set i j (delta + 1)
  in 
  let rec go = function 
    | Leaf i -> (0, [i]) 
    | Q children -> 
       let res = List.map go children in 
       let frontiers = List.map snd res in 
       let delta = 1 + List.fold_left max 0 (List.map fst res) in 
       annotate_q delta frontiers;
       (delta+1, List.concat frontiers) 
    | P children -> 
       let res = List.map go children in 
       let frontiers = List.map snd res in 
       let delta = 1 + List.fold_left max 0 (List.map fst res) in 
       annotate_p delta frontiers;
       (delta, List.concat frontiers) 
  in 
  ignore (go pqtree);
  let elements = DataStruct.MoreList.range 0 (n-1) in
  let d i j = array.(i).(j)  in
  Dissimilarity.({ elements; d})
          

let uniform n = 
  from_pqtree (PqTree.sample n)

  


let pqtree_from_intervals ~n ~k =
  let pqtree = 
    if n = 1 then Leaf 0
    else P (DataStruct.MoreList.range 0 (n-1) |> List.map (fun i -> Leaf i)) 
  in 
  let index i = i in 
  let algo = ImperativeBL.get_algorithms pqtree index in
  let insert_interval () =
    algo.get () 
    |> PqTree.sample_interval
    |> algo.insert 
    |> ignore
  in 
  for _ = 1 to k do insert_interval () done;
  algo.get ()
              

let from_intervals ~n ~k = 
  from_pqtree (pqtree_from_intervals ~n ~k)
       

