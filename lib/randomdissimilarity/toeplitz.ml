open DissimilarityLib

let toeplitz diagonals =
  if diagonals = [] then 
    invalid_arg "toeplitz []";
  let coefs =
    ((1,0) :: diagonals)
    |> List.map (fun (l,coef) -> Array.make l coef) 
    |> Array.concat
  in 
  let n = Array.length coefs in 
  let elements = DataStruct.MoreList.range 0 (n-1) in 
  let d i j =  coefs.(abs (i-j)) in
  Dissimilarity.({ elements; d })


let toeplitz012 ~n ~k = 
  toeplitz [(k,1); (n-k-1,2)]



