open DissimilarityLib
open PqTreeLib

val from_pqtree : int PqTree.t -> int Dissimilarity.t 

val uniform : int -> int Dissimilarity.t

val pqtree_from_intervals : n:int -> k:int -> int PqTree.t 

val from_intervals : n:int -> k:int -> int Dissimilarity.t 
