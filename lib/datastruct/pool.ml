
type 'elt t = ('elt list * 'elt Seq.t)

let empty available = ([], available )

exception Empty_pool 

let get = function 
  | (h::t, s) -> (h,(t,s))
  | ([], seq) -> 
     begin match seq () with 
       | Seq.Nil -> raise Empty_pool
       | Seq.Cons (h,s) -> (h, ([],s))
     end


let free elt (l,s) = (elt::l,s)

let make_int_pool size =
  empty (Seq.unfold (fun i -> if i < size then Some (i,i+1) else None) 0)


let infinite_int_pool = 
  empty (Seq.unfold (fun i -> Some (i, i+1)) 0)
