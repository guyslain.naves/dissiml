module Make = functor (K : Map.OrderedType) -> 
struct 

  module M = Map.Make(K) 

  let add_to_list key value map = 
    M.update key 
      (function 
      | None -> Some [value]
      | Some list -> Some (value::list)
      )
      map 

  let insert ~get_key map value = 
    add_to_list (get_key value) value map 
      
  let sort ~get_key values = 
    values
    |> List.fold_left (insert ~get_key) M.empty 
    |> M.bindings 
    |> List.map snd

end 
