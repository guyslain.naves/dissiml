type 'elt t 

val empty : 'elt t 

val is_empty : 'elt t -> bool 

val push_left : 'elt -> 'elt t -> 'elt t 

val push_right : 'elt t -> 'elt -> 'elt t 

val view_left : 'elt t -> ('elt * 'elt t) option 

val view_right : 'elt t -> ('elt t * 'elt) option

val to_list : 'elt t -> 'elt list 

val of_list : 'elt list -> 'elt t 

val reverse : 'elt t -> 'elt t 

val append_to_left : 'elt t -> 'elt t -> 'elt t 

val append_to_right : 'elt t -> 'elt t -> 'elt t 



val singleton : 'elt -> 'elt t 

val (@>) :  'elt t -> 'elt t -> 'elt t 

val (@<) : 'elt t -> 'elt t -> 'elt t 


val map : ('elt -> 'im) -> 'elt t -> 'im t

val first : 'elt t -> 'elt 
val last : 'elt t -> 'elt 
val pop_first : 'elt t -> 'elt t 
val pop_last : 'elt t -> 'elt t 

val fold_lr : ('accu -> 'elt -> 'accu) -> 'accu -> 'elt t -> 'accu 
val fold_rl : ('elt -> 'accu -> 'accu) -> 'elt t -> 'accu -> 'accu 

val for_all : ('elt -> bool) -> 'elt t -> bool 

val filter : ('elt -> bool) -> 'elt t -> 'elt t 

val concat_map : ('elt -> 'im t) -> 'elt t -> 'im t 

val consecutives : 'elt t -> ('elt * 'elt) t 

