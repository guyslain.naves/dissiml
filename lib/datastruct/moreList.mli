val range : int -> int -> int list 

val take_while : f:('elt -> bool) -> 'elt list -> ('elt list * 'elt list)

val group_by : f:('elt -> 'comparable) -> 'elt list -> 'elt list list

val sort_and_group_by : f:('elt -> 'comparable) -> 'elt list -> 'elt list list

val extend : 'elt list -> ('elt * 'elt list * 'elt list) list

val flatten_to : 'elt list list -> 'elt list -> 'elt list

val flat_map : ('elt -> 'im list) -> 'elt list -> 'im list

val last : 'elt list -> 'elt option

val filter_opt : ('elt -> 'im option) -> 'elt list -> 'im list

val consecutives : 'elt list -> ('elt * 'elt) list


val drop : int -> 'elt list -> 'elt list 

val take : int -> 'elt list -> 'elt list 


val suffixes : 'elt list -> 'elt list list 

val strict_suffixes : 'elt list -> 'elt list list 

val prefixes : 'elt list -> 'elt list list 

val strict_prefixes : 'elt list -> 'elt list list 

val factors : 'elt list -> 'elt list list 

val strict_factors : 'elt list -> 'elt list list


val shuffle : 'elt list -> 'elt list
