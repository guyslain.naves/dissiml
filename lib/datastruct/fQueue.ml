
type 'elt t = 'elt list * 'elt list 

let empty = ([], [])

let enqueue queue elt = match queue with
  | ([],[]) -> ([elt],[]) 
  | (pre,post) -> (pre,elt::post) 
  
let is_empty (pre,_) = pre = [] 

let view = function 
  | ([],_) -> None 
  | ([first],others) -> Some (first, (List.rev others,[]))
  | (first::pre,post) -> Some (first, (pre,post))

let of_list list = (List.rev list, [])
