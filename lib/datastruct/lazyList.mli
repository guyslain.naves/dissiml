
module PossiblyFinite :
sig
  type 'elt t = 'elt node Lazy.t 
  and 'elt node = 
    | Nil 
    | Cons of 'elt * 'elt t


  val unfold : ('elt -> 'elt option) -> 'elt -> 'elt t 

  val view : 'elt t -> 'elt node 

  val map : ('elt -> 'im) -> 'elt t -> 'im t 

  val zip_with : ('a -> 'b -> 'im) -> 'a t -> 'b t -> 'im t 

  val take : int -> 'elt t -> 'elt list

  val drop : int -> 'elt t -> 'elt t 

  val head : 'elt t -> 'elt option 

  val get : int -> 'elt t -> 'elt option 

  val append : 'elt list -> 'elt t -> 'elt t 

  val rev_append : 'elt list -> 'elt t -> 'elt t 

  val to_seq : 'elt t -> 'elt Seq.t 

  val of_seq : 'elt Seq.t -> 'elt t 

  val range : int -> int -> int t 

  val ints : int t 
end


module Infinite :
sig 

  type 'elt t = 'elt node Lazy.t 
  and 'elt node = Cons of 'elt * 'elt t


  val unfold : ('elt -> 'elt) -> 'elt -> 'elt t 

  val unfoldl : ('elt -> 'elt) -> 'elt Lazy.t -> 'elt t 

  val const : 'elt -> 'elt t 

  val append : 'elt list -> 'elt t -> 'elt t 

  val view : 'elt t -> 'elt node 

  val map : ('elt -> 'im) -> 'elt t -> 'im t 

  val zip_with : ('a -> 'b -> 'im) -> 'a t -> 'b t -> 'im t 

  val take : int -> 'elt t -> 'elt list

  val lazy_take : int -> 'elt t -> 'elt PossiblyFinite.t 

  val drop : int -> 'elt t -> 'elt t 

  val head : 'elt t -> 'elt

  val get : int -> 'elt t -> 'elt 

  val to_seq : 'elt t -> 'elt Seq.t 

  val ints : int t 

  val fix : ('elt t -> 'elt t) -> 'elt t
end 
