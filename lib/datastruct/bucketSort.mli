module Make :
  functor (K : Map.OrderedType) ->
    sig
      val sort : get_key:('a -> K.t) -> 'a list -> 'a list list
    end
