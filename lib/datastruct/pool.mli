type 'elt t

val empty : 'elt Seq.t -> 'elt t

val get  : 'elt t -> 'elt * 'elt t 

val free : 'elt -> 'elt t -> 'elt t


val make_int_pool : int -> int t 

val infinite_int_pool : int t 
