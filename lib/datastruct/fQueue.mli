type 'elt t 

val empty : 'elt t 

val enqueue : 'elt t -> 'elt -> 'elt t 

val view : 'elt t -> ('elt * 'elt t) option 

val is_empty : 'elt t -> bool 

val of_list : 'elt list -> 'elt t 
