module type DoubleEndedQueue = 
sig 
  type 'elt t 

  val empty : 'elt t 

  val is_empty : 'elt t -> bool 

  val push_left : 'elt -> 'elt t -> 'elt t 

  val push_right : 'elt t -> 'elt -> 'elt t 

  val view_left : 'elt t -> ('elt * 'elt t) option 

  val view_right : 'elt t -> ('elt t * 'elt) option

  val to_list : 'elt t -> 'elt list 

  val of_list : 'elt list -> 'elt t 

  val reverse : 'elt t -> 'elt t 

  val append_to_left : 'elt t -> 'elt t -> 'elt t 

  val append_to_right : 'elt t -> 'elt t -> 'elt t 
end 



module NaiveQueue : DoubleEndedQueue = 
struct 

  type 'elt t = 'elt list * 'elt list 

  let empty = ([], [])

  let is_empty = function 
  | ([], []) -> true
  | _ -> false 

  let push_left elt (prefix, suffix) = (elt::prefix, suffix)

  let push_right  (prefix, suffix) elt = (prefix, elt::suffix)

  let rec view_left = function 
  | ([], []) -> None 
  | (head::prefix, suffix) -> Some (head, (prefix, suffix))
  | ([], suffix) -> view_left (List.rev suffix, [])

  let rec view_right = function
  | ([],[]) -> None 
  | (prefix, last::suffix) -> Some ((prefix, suffix), last) 
  | (prefix,[]) -> view_right ([], List.rev prefix)


  let to_list (prefix, suffix) = prefix @ (List.rev suffix)

  let of_list prefix = (prefix, [])

  let reverse (prefix, suffix) = (suffix, prefix)

  let rec append_to_left left right =
    match view_left right with 
    | None -> left 
    | Some (e,q) -> append_to_left (push_right left e) q

  let rec append_to_right left right = 
    match view_right left with 
    | None -> right 
    | Some (q,e) -> append_to_right q (push_left e right)
end


include NaiveQueue


let singleton elt = push_right empty elt 

let (@>) = append_to_right
let (@<) = append_to_left


let map f queue = 
  let rec map_k k queue =
    match view_left queue with 
    | None -> k empty
    | Some (first, others) -> map_k (fun res -> k (push_left (f first) res)) others
  in 
  map_k (fun q -> q) queue 

let first queue = match view_left queue with 
| None -> invalid_arg "first on empty DeQueue"
| Some (e,_) -> e 

let last queue = match view_right queue with 
| None -> invalid_arg "last on empty Dequeue"
| Some (_,e) -> e 

let pop_first queue = match view_left queue with 
| None -> invalid_arg "pop_first on empty DeQueue" 
| Some (_,q) -> q 

let pop_last queue = match view_right queue with 
| None -> invalid_arg "pop_last on empty DeQueue"
| Some (q,_) -> q 



let rec fold_lr f accu queue = match view_left queue with 
| None -> accu 
| Some (e,q) -> fold_lr f (f accu e) q 
 
let rec fold_rl f queue accu = match view_right queue with 
| None -> accu 
| Some (q,e) -> fold_rl f q (f e accu)


let filter test queue = 
  fold_lr (fun q e -> if test e then push_right q e else q) empty queue 


let concat_map f queue = 
  fold_lr (fun q e -> append_to_left q (f e)) empty queue 


let rec for_all test queue = 
  match view_left queue with 
  | None -> true 
  | Some (e,_) when not (test e) -> false 
  | Some (_,q) -> for_all test q


let zip queue1 queue2 = 
  let rec zip_k k queue1 queue2 = 
    match view_left queue1, view_left queue2 with 
    | Some (e1,q1), Some (e2,q2) -> zip_k (fun res -> k (push_left (e1,e2) res)) q1 q2
    | _,_ -> k empty 
  in 
  zip_k (fun q -> q) queue1 queue2 
                                      

let consecutives queue = 
  if is_empty queue then empty 
  else zip (pop_last queue) (pop_first queue)
