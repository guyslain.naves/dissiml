let range a b = 
  let rec go suffix current = 
    if current < a then suffix 
    else go (current::suffix) (current-1)
  in 
  go [] b


let take_while ~f = 
  let rec go prefix = function
    | head :: tail when f head ->
       go (head::prefix) tail
    | list -> (List.rev prefix, list)
  in 
  go []


let group_by ~f = 
  let rec go groups = function
  | [] -> List.rev groups
  | head::tail -> 
     let img = f head in 
     let (prefix,suffix) = take_while ~f:(fun x -> f x = img) tail in 
     go ((head::prefix)::groups) suffix
  in 
  go []


let sort_and_group_by ~f list =
  list
  |> List.map (fun elt -> (elt, f elt))
  |> List.sort (fun (_,img1) (_,img2) -> compare img1 img2)
  |> group_by ~f:snd
  |> List.map (List.map fst)
    

let extend = fun list ->
  let rec go before = function
    | [] -> []
    | head::tail -> (head, before, tail) :: go (head::before) tail
  in 
  go [] list

    
let flatten_to lists tail = 
  List.rev_append
    (List.fold_left (fun l p -> List.rev_append p l) [] lists)
    tail


let flat_map f list = 
  list 
  |> List.map f
  |> List.flatten


let last l = List.fold_left (fun _ l -> Some l) None l


let filter_opt f list = 
  list 
  |> List.map f 
  |> List.filter Option.is_some 
  |> List.map Option.get 


let consecutives = function
  | [] -> []
  | first :: others -> 
     List.fold_left 
       (fun (pairs, previous) next -> ((previous,next)::pairs, next))
       ([], first)
       others 
     |> fst 
     |> List.rev 


let rec drop i = function
  | _::tail when i > 0 -> drop (i-1) tail 
  | list -> list 

let take i list = 
  let rec go accu i = function 
  | hd::tail  when i > 0 -> go (hd::accu) (i-1) tail 
  | _ -> List.rev accu
  in 
  go [] i list 


let strict_suffixes list = 
  let rec go accu = function
    | [] -> List.rev accu
    | (_::tail) as list -> go (list::accu) tail
  in 
  go [] list 

let suffixes list = [] :: strict_suffixes list 


let prefixes, strict_prefixes =
  let cons e l = e :: l in 
  let rec go k = function 
    | [] -> k [[]]
    | head::tail -> go (fun prefs -> k ([] :: List.map (cons head) prefs)) tail
  in 
  (fun list -> go (fun l -> l) list),
  (function 
   | [] -> [] 
   | head::tail -> go (List.map (cons head)) tail 
  )


let strict_factors list = 
  list 
  |> prefixes 
  |> flat_map strict_suffixes 


let factors list = [] :: strict_factors list




let rec shuffle = function 
  | [] -> [] 
  | [_] as list -> list 
  | list -> 
     let (left,right) = List.partition (fun _ -> Random.bool()) list in 
     List.rev_append (shuffle left) (shuffle right)
  
