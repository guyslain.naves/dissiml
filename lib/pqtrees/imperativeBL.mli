
type 'elt algorithms = 
  { is_interval : 'elt list -> bool;
    is_block : 'elt list -> bool;
    insert : 'elt list -> bool;
    get : unit -> 'elt PqTree.t
  }


val get_algorithms : 'elt PqTree.t -> ('elt -> int) -> 'elt algorithms
                


module type S =
  sig 
    type elt 
    val pqtree : elt PqTree.t
    val index : elt -> int 
end 


module type BLSig = 
sig
   
  type elt 
     
  val is_interval : elt list -> bool 

  val is_block : elt list -> bool

  val insert : elt list -> bool 

  val get : unit -> elt PqTree.t 

  module Test : sig 

    val write_tree : Format.formatter -> unit

  end

end


val get_bl_module : (module S with type elt = 'e) -> (module BLSig with type elt = 'e) 

