open! DataStruct
open PqTree 

type ('elt,'annotation) annot_tree =
  | PA of ('elt,'annotation) annot_tree list * 'annotation
  | QA of ('elt,'annotation) annot_tree list * 'annotation
  | LeafA of 'elt * 'annotation

let root_annot = 
  function 
  | PA (_,annot) 
  | QA (_,annot)
  | LeafA (_,annot) -> annot

let rec erase_annot = function
  | LeafA (elt,_) -> Leaf elt 
  | PA (children, _) -> P (List.map erase_annot children) 
  | QA (children, _) -> Q (List.map erase_annot children)


let count_pertinent_leaves is_pertinent_leaf root = 
  let sum_annot children =
    children
    |> List.map root_annot 
    |> List.fold_left (+) 0
  in 
  let rec go = function
  | Leaf l when is_pertinent_leaf l -> LeafA (l,1)
  | Leaf l -> LeafA (l, 0)  
  | P children ->
     let annoted_children = List.map go children in 
     PA (annoted_children, sum_annot annoted_children)
  | Q children ->
     let annoted_children = List.map go children in 
     QA (annoted_children, sum_annot annoted_children )
  in 
  go root


exception Reduce_Failure 


type 'elt reduced_node =
  | Empty of  'elt t
  | Full of 'elt t
  | Partial of 'elt t list * 'elt t list


let partition_p children = 
  (MoreList.filter_opt (function Empty t -> Some t | _ -> None) children,
   MoreList.filter_opt (function Partial (empties,fulls) -> Some (empties,fulls) | _ -> None) children,
   MoreList.filter_opt (function Full t -> Some t | _ -> None) children
  )

let join_p = function
  | [] -> raise Reduce_Failure
  | [single] -> single
  | children -> P children

let q_from_partial (empty_children, full_children) = 
  Q (empty_children @ (List.rev full_children))
 
let reduce_p_node = function 
  | [], [], full_children ->  
     Full (join_p full_children)
  | empty_children, [], full_children ->
     Partial ([join_p empty_children], [join_p  full_children])
  | empty_children, [(empty_grand,full_grand)], full_children ->
     let empties = 
       if empty_children = [] then empty_grand 
       else join_p empty_children :: empty_grand 
     in
     let fulls = 
       if full_children = [] then full_grand 
       else join_p full_children :: full_grand 
     in 
     Partial (empties, fulls)
  | _ ->
     raise Reduce_Failure 

let reduce_pertinent_p_node = function   
  | [], [], full_children ->  
     join_p full_children
  | empty_children, [], full_children ->
     P (join_p full_children :: empty_children)
  | empty_children, [(empty_grand, full_grand)], full_children ->
     let t = join_p full_children in 
     let s = q_from_partial (empty_grand, t :: full_grand) in 
     join_p (s :: empty_children)
  | empty_children, 
    [(empty_grand1,full_grand1); (empty_grand2, full_grand2)], 
    full_children ->
     let s_children = 
       List.concat [ 
           empty_grand1;
           List.rev full_grand1;
           (if full_children = [] then [] else [join_p full_children]);
           full_grand2;
           List.rev empty_grand2
         ]
     in
     join_p ( (Q s_children) :: empty_children)
  | _ ->
     raise Reduce_Failure 


type 'elt seq =
  | EmptySeq of 'elt t list 
  | FullSeq of 'elt t list 

let from_tagged = function
  | Empty tree 
  | Full tree -> tree 
  | Partial (empties,fulls) -> Q (empties @ List.rev fulls)


let push_full seq tree = 
  match seq with
  | FullSeq list :: seqs -> FullSeq (tree::list) :: seqs
  | seqs ->  FullSeq [tree] :: seqs 

let push_empty seq tree = 
  match seq with
  | EmptySeq list :: seqs -> EmptySeq (tree :: list) :: seqs 
  | seqs -> EmptySeq [tree] :: seqs


let partition_q trees = 
  let rec consume_full seq = function 
    | [] -> seq
    | Full child :: others -> 
       consume_full (push_full seq child) others
    | Partial (empties,fulls) :: others -> 
       consume_empty 
         (EmptySeq empties :: List.fold_left push_full seq fulls)
         others 
    | children -> consume_empty seq children 
  and consume_empty seq = function 
    | [] -> seq 
    | Empty child :: others -> 
       consume_empty (push_empty seq child) others
    | Partial (empties, fulls) :: others ->
       consume_full 
         (FullSeq fulls :: List.fold_left push_empty seq empties)
         others 
    | children -> consume_full seq children
  in
  let rec starts_with_empty = function 
    | [] -> true
    | Empty _ :: _ -> true 
    | Full _ :: _-> false 
    | Partial _ :: tail -> not (starts_with_empty tail)
  in
  trees 
  |> (if starts_with_empty trees then consume_empty [] else consume_full [])
  |> List.map 
       (function EmptySeq seq -> EmptySeq (List.rev seq)
               | FullSeq seq -> FullSeq (List.rev seq)
       )
  |> List.rev 



let reduce_q_node = function 
    | [ FullSeq fulls ] -> 
       Full (Q fulls)
    | [ EmptySeq emptys; FullSeq fulls] ->
       Partial (emptys, List.rev fulls)
    | [ FullSeq fulls; EmptySeq empties] ->
       Partial (List.rev empties, fulls)
    | _ ->
       raise Reduce_Failure

let reduce_pertinent_q_node = function 
    | [ EmptySeq empties1; FullSeq fulls; EmptySeq empties2 ] ->
       let children =
         List.concat [ empties1; fulls; empties2]
       in
       Q children
    | partition -> 
       partition 
       |> reduce_q_node 
       |> from_tagged



let rec reduce = function  
  | tree when root_annot tree = 0 -> 
     Empty (erase_annot tree)
  | LeafA (elt,_) -> Full (Leaf elt)
  | PA (children, _) -> 
     children
     |> List.map reduce
     |> partition_p 
     |> reduce_p_node  
  | QA (children, _) -> 
     children
     |> List.map reduce 
     |> partition_q
     |> reduce_q_node 

let reduce_pertinent_node = function 
  | PA (children, _) ->
     children 
     |> List.map reduce 
     |> partition_p
     |> reduce_pertinent_p_node
  | QA (children, _) -> 
     children
     |> List.map reduce 
     |> partition_q 
     |> reduce_pertinent_q_node 
  | LeafA (elt,_) -> Leaf elt



let is_pertinent_node nb_pertinent_leaves root =
  match root with 
  | LeafA (_,count) -> count = 1 && nb_pertinent_leaves = 1
  | PA (children, count) | QA (children, count) ->
     count == nb_pertinent_leaves &&
       let non_empty_children = List.filter (fun child -> root_annot child >= 1) children in 
       List.length non_empty_children > 1

let rec find_and_reduce_pertinent_node nb_pertinent_leaves tree = 
  match tree with
  | LeafA _ | PA (_, 0) | QA (_, 0) -> 
     erase_annot tree 
  | tree when is_pertinent_node nb_pertinent_leaves tree -> 
     reduce_pertinent_node tree 
  | PA (children, _) ->
     P (List.map (find_and_reduce_pertinent_node nb_pertinent_leaves) children) 
  | QA (children, _) -> 
     Q (List.map (find_and_reduce_pertinent_node nb_pertinent_leaves) children)
           


let add_interval is_in_interval interval_length tree =
  try 
    tree 
    |> count_pertinent_leaves is_in_interval 
    |> find_and_reduce_pertinent_node interval_length
    |> Option.some
  with 
  | Reduce_Failure -> None
