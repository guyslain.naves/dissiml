(** This modules contains an algorithm that, given a PQ-tree T on X,
    and a chain S_0 = {s}, S_1, ... S_k = X, builds a PQ-tree
    representing all the permutations represented by T and for which
    each S_i is an interval, if such a PQ-tree exists.

    This could be done by the Booth-Lueker interval insertion
    algorithm (functionalBL or imperativeBL), with k insertions. This
    algorithm improves upon repeated insertion by having a linear-time
    complexity.
*)

open DataStruct 



module IntPair = 
struct 
  type t = (int * int) 
  let compare (x1,y1) (x2,y2) =
    let diff = x1 - x2 in 
    if diff = 0 then y1 - y2 
    else diff
end
module IntPairSorter = BucketSort.Make(IntPair)
module IntPairMap = Map.Make(IntPair)




module Bound = struct
  type t = 
  | Infinity
  | Bounded of int 


  let at_most value = Bounded value 

  let compare bound1 bound2 = 
    match bound1, bound2 with
    | Bounded _, Infinity -> -1
    | Infinity, Bounded _ -> 1
    | Bounded b1, Bounded b2 -> Int.compare b1 b2
    | Infinity, Infinity -> 0

  let min bound1 bound2 = 
    if compare bound1 bound2 <= 0 then bound1 else bound2

  let is_bounded value = function 
  | Infinity -> true
  | Bounded b -> value <= b


end

(* We parametrize the whole algorithm by the chain, encoded by a rank function d,
   d(x) is such that x \in S_{d(x)} \setminus S_{d(x)-1}, and d(s) = 0.
 *)
module Make = 
  functor (Chain : sig type t val s : t val d : t -> int end) ->
  struct
    

    type elt = Chain.t 
    type pqtree = elt PqTree.t

    open Chain 
    open PqTree

    let q_node subtrees = 
      match DeQueue.to_list subtrees with 
      | [single] -> single
      | [_1;_2] as children -> P children
      | children -> Q children 

    type external_subtree = 
      | ConstantSubtree of int * pqtree 
      | IncreasingSubtree of int * pqtree DeQueue.t * int 
      (* an increasing subtree is necessarily a Q-node *)

    let bounds = function 
    | ConstantSubtree (delta,_) -> (delta, delta)
    | IncreasingSubtree (delta_min, _, delta_max) -> (delta_min, delta_max)
    let d_min = function 
    | ConstantSubtree (delta,_) -> delta 
    | IncreasingSubtree (delta,_,_) -> delta 
    let d_max = function 
    | ConstantSubtree (delta, _) -> delta 
    | IncreasingSubtree (_,_,delta) -> delta 

    let tree_of_external_tree = function 
    | ConstantSubtree (_,t) -> t 
    | IncreasingSubtree (_, children,_) -> q_node children


    let is_increasing_sequence subtrees = 
      let open DeQueue in 
      for_all 
        (fun (subtree1, subtree2) -> d_max subtree1 <= d_min subtree2)
        (consecutives subtrees)

    let force_increasing_sequence subtrees = 
      if is_increasing_sequence subtrees then Some subtrees 
      else let reversed = DeQueue.reverse subtrees in 
        if is_increasing_sequence reversed then Some reversed
        else None 

    let expand_increasing_sequence subtrees = 
      let contract = function 
      | ConstantSubtree (_,t) -> DeQueue.singleton t
      | IncreasingSubtree (_, children, _) -> children
      in  
      DeQueue.concat_map contract subtrees


    let sort_into_increasing_sequence subtrees = 
      subtrees 
      |> DeQueue.to_list
      |> IntPairSorter.sort ~get_key:bounds 
      |> DeQueue.of_list


    type sorting_group = 
      | Group of int * pqtree list 
      | Single of external_subtree 

    let group_d_min = function 
    | Group (delta,_) -> delta 
    | Single subtree -> d_min subtree

    let group_d_max = function 
    | Group (delta,_) -> delta 
    | Single subtree -> d_max subtree

    let sort_children_of_p_node children =
      let group = function 
      | [ ConstantSubtree _ as tree ] -> DeQueue.singleton (Single tree)
      | (ConstantSubtree (delta,_)::_) as trees -> 
          DeQueue.singleton ( Group (delta, List.map tree_of_external_tree trees) )
      | trees -> List.map (fun t -> Single t) trees |> DeQueue.of_list
      in
      children
    |> sort_into_increasing_sequence
    |> DeQueue.concat_map group 


    let is_increasing_group_sequence groups = 
      let open DeQueue in
      for_all 
        (fun (group1, group2) -> group_d_max group1 <= group_d_min group2)
        (consecutives groups)

      


    type s_path = 
      | End of elt 
      | PPath of external_subtree DeQueue.t * s_path 
      | QPathIn of external_subtree DeQueue.t * s_path * external_subtree DeQueue.t 
      | QPathOut of s_path * external_subtree DeQueue.t 




    type result = 
      | NotRepresentable
      | CenteredSubtree of s_path
      | ExternalSubtree of external_subtree 

    type classification =
      | Unclassifiable
      | Internal of external_subtree DeQueue.t * s_path * external_subtree DeQueue.t 
      | External of external_subtree DeQueue.t 

    let classify children =
      let open DeQueue in 
      let rec go classification children = 
        match classification, children with 
        | _, [] -> classification
        | Unclassifiable, _
        | _, NotRepresentable::_ -> Unclassifiable
        | Internal (left,path_to_s,right) , ExternalSubtree child::children -> 
            go (Internal (left, path_to_s, push_right right child)) children 
        | External left_of_s, ExternalSubtree child::children ->
            go (External (push_right left_of_s child)) children
        | External left_of_s, CenteredSubtree centered::children ->
            go (Internal (left_of_s, centered, empty)) children 
        | Internal _, CenteredSubtree _ :: _ -> Unclassifiable 
      in 
      go (External empty) children 



          
    type centered_subtree = Central of (int * pqtree DeQueue.t * int)
                                       
    let left_bound_of (Central (b,_,_)) = b
    let right_bound_of (Central (_,_,b)) = b 

    let central_tree (Central (_,subtrees,_)) =
      match DeQueue.view_left subtrees with
      | Some (single,queue) when DeQueue.is_empty queue -> single
      | Some _ -> q_node subtrees 
      | None -> assert false 

    let consolidate (Central (left_bound,_,right_bound) as tree) = 
      Central (left_bound, DeQueue.singleton (central_tree tree), right_bound)

    let reverse (Central (left_bound, subtrees, right_bound)) =
      Central (right_bound, DeQueue.reverse subtrees, left_bound)

    let push_left (Central (_, centrals, right_bound)) = function 
    | Group (delta, pqtrees) ->  
        Central (delta, DeQueue.push_left (PqTree.P pqtrees) centrals, right_bound)
    | Single (ConstantSubtree (delta, pqtree)) ->
        Central (delta, DeQueue.push_left pqtree centrals,right_bound)
    | Single (IncreasingSubtree (_, pqtrees, dmax)) ->
        Central (dmax, DeQueue.(reverse pqtrees @> centrals), right_bound)

    let push_right (Central (left_bound, centrals, _)) = function 
    | Group (delta, pqtrees) ->
        Central (left_bound, DeQueue.push_right centrals (PqTree.P pqtrees), delta)
    | Single (ConstantSubtree (delta, pqtree)) ->
        Central (left_bound, DeQueue.push_right centrals pqtree, delta)
    | Single (IncreasingSubtree (_,pqtrees,dmax)) -> 
        Central (left_bound, DeQueue.(centrals @< pqtrees), dmax)
                       
    let push_any_side prefers_left (Central (l,centrals,r) as central) = function 
    | Group (delta, pqtrees) -> 
        Central (delta, DeQueue.singleton (P (DeQueue.first centrals :: pqtrees)), delta)
    | Single (ConstantSubtree (delta, pqtree)) when delta = l && delta = r ->
        Central (delta, DeQueue.singleton (P [DeQueue.first centrals; pqtree]), delta)
    | group when prefers_left -> push_left central group
    | group -> push_right central group


    let push bound_left bound_right central group =
      let d_min = group_d_min group and d_max = group_d_max group in 
      let central = 
        if d_min >= max (left_bound_of central) (right_bound_of central) 
        && Bound.is_bounded d_min bound_left && Bound.is_bounded d_min bound_right then 
          consolidate central 
        else 
          central
      in          
      let no_overleft = Bound.is_bounded d_max bound_left in 
      let no_overright = Bound.is_bounded d_max bound_right in 
      let is_left_compatible = d_min >= left_bound_of central && no_overleft in 
      let is_right_compatible = d_min >= right_bound_of central && no_overright in 
      let prefers_left = left_bound_of central >= right_bound_of central in 
      if is_left_compatible && is_right_compatible then 
        Some (push_any_side prefers_left central group)
      else if is_left_compatible then 
        Some (push_left central group)
      else if is_right_compatible then 
        Some (push_right central group) 
      else if d_min >= left_bound_of central && no_overright then 
        Some (push_right (reverse central) group)
      else if d_min >= right_bound_of central && no_overleft then 
        Some (push_left (reverse central) group)
      else 
        None 

    let push_all_groups bound_left bound_right central sorted_groups = 
      let optional_push optional_central group = 
        match optional_central with 
        | Some central -> push bound_left bound_right central group 
        | None -> None 
      in       
      DeQueue.fold_lr optional_push (Some central) sorted_groups

 
    let build_p_internal left_bound right_bound children center_child =
      push_all_groups left_bound right_bound center_child children





    let contract_group = function 
    | Group (_, subtrees) -> DeQueue.singleton (P subtrees)
    | Single (ConstantSubtree (_,subtree)) -> DeQueue.singleton (subtree)
    | Single (IncreasingSubtree (_,subtrees,_)) -> subtrees 
    
    let build_p_external unsorted_subtrees = 
      let groups = sort_children_of_p_node unsorted_subtrees in 
      match DeQueue.view_left groups with
      | _ when not (is_increasing_group_sequence groups) -> NotRepresentable
      | Some (Group (delta,children),q) when DeQueue.is_empty q -> 
          ExternalSubtree (ConstantSubtree (delta, P children))
      | _ ->
          let delta_min = group_d_min (DeQueue.first groups) in 
          let delta_max = group_d_max (DeQueue.last groups) in 
          let subtrees = DeQueue.concat_map contract_group groups in 
          ExternalSubtree (IncreasingSubtree (delta_min, subtrees, delta_max))



    let build_q_external subtrees = 
      match force_increasing_sequence subtrees with 
      | None -> NotRepresentable
      | Some subtrees -> 
          ExternalSubtree (
            let delta_min = d_min (DeQueue.first subtrees) in 
            let delta_max = d_max (DeQueue.last subtrees) in 
            if delta_min = delta_max then 
              ConstantSubtree (delta_min, q_node (DeQueue.map tree_of_external_tree subtrees))
            else  
              IncreasingSubtree (delta_min, expand_increasing_sequence subtrees, delta_max)
          )


    let dispatch root children =
      match root, classify children with 
      | Leaf x, _ when x = s -> 
          CenteredSubtree (End s)
      | Leaf x, _ -> 
          ExternalSubtree (ConstantSubtree (d x, Leaf x))
      | _, Unclassifiable -> NotRepresentable
      | P _, Internal (left_of_s, at_s, right_of_s) -> 
          CenteredSubtree (PPath (DeQueue.append_to_left left_of_s right_of_s, at_s))
      | P _, External subtrees -> 
          build_p_external subtrees 
      | Q _, Internal (left_of_s, at_s, right_of_s) ->
          CenteredSubtree (
            if DeQueue.is_empty left_of_s then 
              QPathOut (at_s, right_of_s)
            else if DeQueue.is_empty right_of_s then  
              QPathOut (at_s, DeQueue.reverse left_of_s)
            else
              QPathIn (DeQueue.reverse left_of_s, at_s, right_of_s)
          )
      | Q _, External subtrees -> 
          build_q_external subtrees
    

    let children = function 
    | Leaf _ -> []
    | P children 
    | Q children -> children 

    let rec make_s_path tree = 
      children tree
      |> List.map make_s_path
      |> dispatch tree


    let collapse bound1 bound2 (Central (left, trees, right)) =
      let max_d = max left right in 
      if Bound.is_bounded max_d bound1 && Bound.is_bounded max_d bound2 then 
        Some (left, DeQueue.singleton (q_node trees), right) 
      else Some (left,trees,right)


    let rec fold_path left_bound right_bound = 
      let (>>=) e f = Option.bind e f in 
      function 
    | End s -> Some (Central (0, DeQueue.singleton (Leaf s), 0)) 
    | PPath (subtrees, path_to_s) -> 
        let sorted_subtrees = sort_children_of_p_node subtrees in 
        let new_bound = Bound.at_most (group_d_min (DeQueue.first sorted_subtrees)) in
        let s_subtree = 
          if Bound.compare left_bound right_bound <= 0 then 
            fold_path (Bound.min new_bound left_bound) right_bound path_to_s
          else
            fold_path left_bound (Bound.min new_bound right_bound) path_to_s
        in
        Option.bind 
          s_subtree 
          (build_p_internal left_bound right_bound sorted_subtrees)
    | QPathOut (path_to_s, subtrees) -> 
        if not (is_increasing_sequence subtrees) then None else
        let new_bound = Bound.at_most (d_min (DeQueue.first subtrees)) in 
        let max_d = d_max (DeQueue.last subtrees) in 
        let fits_left = Bound.is_bounded max_d left_bound in 
        let fits_right = Bound.is_bounded max_d right_bound in 
        let children = expand_increasing_sequence subtrees in 
        if fits_left && (Bound.compare left_bound right_bound <= 0 || not fits_right) then 
          fold_path new_bound right_bound path_to_s >>= 
          collapse new_bound right_bound >>= fun (_,trees, right) -> 
          Some (Central (max_d, DeQueue.(reverse children @> trees), right))
        else if fits_right && (compare left_bound right_bound >= 0 || not fits_left) then 
          fold_path left_bound new_bound path_to_s >>= fun (Central (left,trees,_)) ->
          Some (Central (left, DeQueue.(trees @< children), max_d))
        else None

    | QPathIn (left_subtrees, path_to_s, right_subtrees) ->
        if not (is_increasing_sequence left_subtrees) 
        || not (is_increasing_sequence right_subtrees)
        then None else
        let new_bound_left = Bound.Bounded (d_min (DeQueue.first left_subtrees)) in
        let new_bound_right = Bound.Bounded (d_min (DeQueue.first right_subtrees)) in 
        let max_d_left = d_max (DeQueue.last left_subtrees) in 
        let max_d_right = d_max (DeQueue.last right_subtrees) in 
        let left_children = expand_increasing_sequence left_subtrees in 
        let right_children = expand_increasing_sequence right_subtrees in 
        if Bound.(is_bounded max_d_left left_bound && is_bounded max_d_right right_bound) then 
          fold_path new_bound_left new_bound_right path_to_s >>= 
          collapse new_bound_left new_bound_right >>= fun (_,trees,_) -> 
          Some (Central (max_d_left, DeQueue.(reverse left_children @> trees @< right_children), max_d_right))
        else if Bound.(is_bounded max_d_right left_bound && is_bounded max_d_left right_bound) then
          fold_path new_bound_right new_bound_left path_to_s >>= 
          collapse new_bound_right new_bound_left >>= fun (_, trees, _) ->
          Some (Central (max_d_right, DeQueue.(reverse right_children @> trees @< left_children), max_d_left))
        else 
          None



    let insert_chain tree = 
      match make_s_path tree with 
      | NotRepresentable -> None 
      | CenteredSubtree path_to_s -> 
          path_to_s
          |> fold_path Infinity Infinity
          |> Option.map central_tree
      | ExternalSubtree (ConstantSubtree (_, tree)) -> Some tree 
      | ExternalSubtree (IncreasingSubtree (_, trees, _)) -> Some (q_node trees)


  end


let refine_by_distances (type elt) source dist tree = 
  let module Chain = 
  struct 
    type t = elt
    let s = source
    let d = dist 
  end 
  in
  let module I = Make(Chain) in 
  I.insert_chain tree 
    
