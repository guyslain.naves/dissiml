open PqTree

module Make = functor (S : Set.S) -> 
struct 
  type elt = S.elt 
  type pqtree = elt PqTree.t 

  (* type answer =  *)
  (* | Yes  *)
  (* | No of elt list  *)


  type intersection =
  | Empty 
  | All
  | Complete 
  | Compromised

  let rec p_intersect = function
  | []
  | [ Empty ] -> Empty
  | [ Complete ] -> Complete 
  | [ All ] | [All; All]-> All 
  | Empty :: Empty :: tail -> p_intersect (Empty :: tail)
  | All :: All :: All :: tail -> p_intersect (All :: All :: tail)
  | All :: All :: _  -> Compromised
  | (All | Complete) :: tail 
  | Empty :: (All | Complete) :: tail ->
      if List.for_all (fun elt -> elt = Empty) tail then Complete
      else Compromised
  | Compromised :: _ 
  | _ :: Compromised :: _ -> Compromised

  let rec q_intersect = function 
  | All :: All :: tail -> q_intersect (All :: tail)
  | Empty :: Empty :: tail -> q_intersect (Empty :: tail)
  | Empty :: All :: All :: tail -> q_intersect (Empty :: All :: tail)
  | All :: Empty :: tail 
  | Empty :: All :: Empty :: tail
  | Empty :: Complete :: tail 
  | Complete :: Empty :: tail -> q_intersect(Complete :: tail)
  | [ All ] -> All 
  | [ Empty ] | [] -> Empty 
  | [ Complete ] | [Empty; All] -> Complete
  | All :: Complete :: _ 
  | Compromised :: _
  | _ :: Compromised :: _
  | Empty :: All :: (Complete | Compromised) :: _
  | Complete :: All :: _
  | Complete :: Complete :: _ -> Compromised
  (* | _ -> Compromised  *)
  


  let rec intersect_block block = function 
  | P children -> 
      children
      |> List.map (intersect_block block)
      |> p_intersect 
  | Q children ->
      children
      |> List.map (intersect_block block)
        |> q_intersect 
  | Leaf x when S.mem x block -> All
  | Leaf _ -> Empty  


  let is_a_block tree block = 
    intersect_block block tree <> Compromised

  
  let is_not_singleton set =
    S.cardinal set <> 1

  let rec blocks_and_block = 
    let recurse children = 
        List.to_seq children
        |> Seq.map blocks_and_block 
        |> Seq.split 
    in 
    function 
    | Leaf x -> (Seq.empty, S.singleton x) 
    | P children ->
        let (blocks, sets) = recurse children in 
        (blocks
         |> Seq.concat
         |> Seq.append (Seq.filter is_not_singleton sets), 
         Seq.fold_left S.union S.empty sets
        )
    | Q children ->
        let n = List.length children in 
        let (blocks, sets) = recurse children in 
        let set = Seq.fold_left S.union S.empty sets in 
        let leftist_blocks =
          Seq.scan S.union S.empty sets |> Seq.drop 2 |> Seq.take (n-2) 
        in
        let rightist_blocks = 
          Seq.scan S.diff set sets |> Seq.drop 1 |> Seq.take (n-2)
        in   
        (blocks
         |> Seq.concat 
         |> Seq.append (Seq.filter is_not_singleton sets)
         |> Seq.append leftist_blocks
         |> Seq.append rightist_blocks, 
         set)

  let enumerate_non_trivial_blocks tree = 
    fst (blocks_and_block tree)
  

  let is_refinement tree1 tree2 = 
    Seq.for_all 
      (is_a_block tree1)
      (enumerate_non_trivial_blocks tree2)


  (* let is_refinement tree1 tree2 =  *)
  (*   match certify_refinement tree1 tree2 with  *)
  (*   | Yes -> true *)
  (*   | No _ -> false  *)

end
