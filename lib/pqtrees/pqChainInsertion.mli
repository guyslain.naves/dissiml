open DataStruct

module Bound :
  sig
    type t = Infinity | Bounded of int
    val at_most : int -> t
    val compare : t -> t -> int
    val min : t -> t -> t
    val is_bounded : int -> t -> bool
  end

module Make :
  functor (Chain : sig type t val s : t val d : t -> int end) ->
    sig
      type elt = Chain.t
      type pqtree = elt PqTree.t

      val q_node : pqtree DeQueue.t -> pqtree
      
      type external_subtree = 
      | ConstantSubtree of int * pqtree 
      | IncreasingSubtree of int * pqtree DeQueue.t * int 


      val bounds : external_subtree -> (int * int)
      val d_min : external_subtree -> int
      val d_max : external_subtree -> int 
      val tree_of_external_tree : external_subtree -> pqtree

      val is_increasing_sequence : external_subtree DeQueue.t -> bool 
      val force_increasing_sequence : external_subtree DeQueue.t -> external_subtree DeQueue.t option
      val expand_increasing_sequence : external_subtree DeQueue.t -> pqtree DeQueue.t 
      val sort_into_increasing_sequence : external_subtree DeQueue.t -> external_subtree list DeQueue.t

      type sorting_group = 
      | Group of int * pqtree list 
      | Single of external_subtree 

      val group_d_min : sorting_group -> int 
      val group_d_max : sorting_group -> int 
      val sort_children_of_p_node : external_subtree DeQueue.t -> sorting_group DeQueue.t 
      val is_increasing_group_sequence : sorting_group DeQueue.t -> bool


      type s_path = 
      | End of elt 
      | PPath of external_subtree DeQueue.t * s_path 
      | QPathIn of external_subtree DeQueue.t * s_path * external_subtree DeQueue.t 
      | QPathOut of s_path * external_subtree DeQueue.t 

      type result = 
      | NotRepresentable
      | CenteredSubtree of s_path
      | ExternalSubtree of external_subtree 

      type classification =
      | Unclassifiable
      | Internal of external_subtree DeQueue.t * s_path * external_subtree DeQueue.t 
      | External of external_subtree DeQueue.t 

      val classify : result list -> classification 

          
      type centered_subtree = Central of (int * pqtree DeQueue.t * int)

      val left_bound_of : centered_subtree -> int
      val right_bound_of : centered_subtree -> int
      val central_tree : centered_subtree -> pqtree
      val consolidate : centered_subtree -> centered_subtree
      val reverse : centered_subtree -> centered_subtree
      val push_left : centered_subtree -> sorting_group -> centered_subtree
      val push_right : centered_subtree -> sorting_group -> centered_subtree
      val push_any_side : bool -> centered_subtree -> sorting_group -> centered_subtree
      val push : Bound.t -> Bound.t -> centered_subtree -> sorting_group -> centered_subtree option
      val push_all_groups : Bound.t -> Bound.t -> centered_subtree -> sorting_group DeQueue.t -> centered_subtree option
      val build_p_internal : Bound.t -> Bound.t -> sorting_group DeQueue.t -> centered_subtree -> centered_subtree option
      val contract_group : sorting_group -> pqtree DeQueue.t 
      val build_p_external : external_subtree DeQueue.t -> result
      val build_q_external : external_subtree DeQueue.t -> result 
      val dispatch : pqtree -> result list -> result
      val children : pqtree -> pqtree list
      val make_s_path : pqtree -> result
      val collapse : Bound.t -> Bound.t -> centered_subtree -> (int * pqtree DeQueue.t * int) option 
      val fold_path : Bound.t -> Bound.t -> s_path -> centered_subtree option
      val insert_chain : pqtree -> pqtree option
    end
val refine_by_distances : 'elt -> ('elt -> int) -> 'elt PqTree.t -> 'elt PqTree.t option
