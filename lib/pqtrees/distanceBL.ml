

type 'elt result = 
  { contains_pivot : bool;
    mini : int;
    maxi : int;
    tree : 'elt PqTree.t;
  }

let from_result result = result.tree 

let is_uniform result = result.mini = result.maxi 


let reduce_leaf d q leaf = 
  if leaf = q then 
    { contains_pivot = true; mini = 0; maxi = 0; tree = Leaf q }
  else 
    { contains_pivot = false;
      mini = d leaf;
      maxi = d leaf;
      tree = Leaf q 
    }


let compare_result result1 result2 = 
  match result1.contains_pivot, result2.contains_pivot with
      | true, true -> 0
      | true, false -> -1 
      | false, true -> 1 
      | false, false -> 
         let delta = compare result1.mini result2.mini  in 
         if delta <> 0 then delta 
         else compare result1.maxi result2.maxi



let order_results (type elt) results =
  let module Key = 
    struct 
      type t = elt result
      let compare = compare_result 
    end 
  in 
  let module IntervalMap = Map.Make(Key) in              
  let insert_map map res = 
    IntervalMap.update 
      res
      (function 
         None -> Some [res]
       | Some list -> Some (res::list)
      )
      map 
  in 
  results 
  |> List.fold_left insert_map IntervalMap.empty 
  |> IntervalMap.to_seq 
  



(* let reduce_p_node d q children_results = 
 *   let children = order_results children_results in 
 *   () *)
