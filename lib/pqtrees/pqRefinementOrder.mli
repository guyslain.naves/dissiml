module Make : functor (S : Set.S) -> 
sig
  type elt = S.elt 
  type pqtree = elt PqTree.t 


  val is_a_block : pqtree -> S.t -> bool

  val enumerate_non_trivial_blocks : pqtree -> S.t Seq.t 

  val is_refinement : pqtree -> pqtree -> bool 


end 



(* type 'elt answer =  *)
(*   | Yes  *)
(*   | No of 'elt list (\** a certifying permutation *\) *)


(* (\** [is_refinement tree1 tree2] checks whether all permutations *)
(*     represented by [tree1] are represented in [tree2]. *\) *)
(* val is_refinement : 'elt PqTree.t -> 'elt PqTree.t -> bool  *)


(* (\** [certify_refinement tree1 tree2] returns [Yes] if [tree1] is a *)
(*     refinement of [tree2], or [No permut] where [permut] is *)
(*     represented in [tree1] but not in [tree2]. *\) *)
(* val certify_refinement : 'elt PqTree.t -> 'elt PqTree.t -> 'elt answer  *)

