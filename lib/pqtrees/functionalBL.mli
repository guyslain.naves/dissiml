
(** [add_interval is_in_subset subset_cardinal pq_tree] returns a
   PQ-tree obtained from [pq_tree] by adding an interval described by
   [is_in_interval] and containing [interval_length] elements.

   @param is_in_subset a subset of elements represented as a predicate,
     [true] for any element in the subset
   @param subset_cardinal the number of elements in the subset
   @param tree a PQ-tree
   @return Some PQ-tree representing the set of permutations that are represented in [tree] and admits the subset as an interval, [None] if no such permutation exists.

 *)
val add_interval : ('elt -> bool) -> int -> 'elt PqTree.t -> 'elt PqTree.t option


