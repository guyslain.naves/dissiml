
let int_enumerator start =
  Seq.unfold (fun b -> Some (b, b+1)) start
  

let rec valuation_2 n = 
  if n mod 2 = 0 then 1 + valuation_2 (n/2) 
  else 0


let gray_code_enumerator = 
  Seq.map valuation_2 (int_enumerator 1)


let subset_enumerator elements =
  let n = List.length elements in 
  let rec flip k elements subset = 
    match elements, subset with  
    | e1::_, s::sub when k = 0 && e1 = s -> sub 
    | e1::_, _ when k = 0 -> e1::subset 
    | e1::elts, s::sub when e1 = s -> s :: flip (k-1) elts sub 
    | _::elts, _ -> flip (k-1) elts subset
    | _ -> assert false 
  in 
  let step (index,subset) =
        let two_val = valuation_2 index in 
        if two_val >= n then None
        else 
          let next_subset = flip two_val elements subset in 
          Some (next_subset, (index+1, next_subset))
  in
  Seq.cons elements (Seq.unfold step (1, []))
  


let rec fact_valuation f n =
  let r = n mod f in 
  if r = 0 then fact_valuation (f+1) (n/f) 
  else (f,r)

let transposition k = 
  let (n,r) = fact_valuation 2 k in 
  if n mod 2 = 0 then (r,n) 
  else (1,n) 

let heap_transposition_enumerator = 
  Seq.map transposition (int_enumerator 1)


let permutation_enumerator elements = 
  let n = List.length elements in 
  let rec replace i e = function
    | f::tail when i = 0 -> (f,e::tail)
    | head::tail -> 
       let (f,tail') =  replace (i-1) e tail in 
       (f, head::tail')
    | [] -> assert false
  in 
  let rec transpose i j = function 
    | ei::tail when i = 0 -> 
       let (ej, tail') = replace (j-1) ei tail in 
       ej::tail'
    | head::tail -> 
       head :: transpose (i-1) (j-1) tail 
    | [] -> assert false
  in 
  let step (index, permutation) =
    let (i,j) = transposition index in 
    if j > n then None
    else 
      let next_permutation = transpose (i-1) (j-1) permutation in 
      Some (next_permutation, (index+1, next_permutation))
  in 
  Seq.cons elements (Seq.unfold step (1, elements))
  


let pair_enumerator enum_a enum_b = 
  let open Seq in 
  let rec step (state_a, current_b, state_b) = 
    match state_a () with 
    | Nil -> 
       begin match state_b () with 
       | Nil -> None 
       | Cons (new_b, new_state_b) -> step (enum_a, new_b, new_state_b)
       end 
    | Cons (new_a, new_state_a) -> 
       Some ((new_a,current_b), (new_state_a, current_b, state_b))
  in 
  match enum_a (), enum_b () with 
  | Cons (a, state_a), Cons (b, state_b) -> 
     Seq.cons (a,b) (Seq.unfold step (state_a,b,state_b))
  | _,_ -> Seq.empty


let (>>=) enum_a a_to_enum_b =
  Seq.flat_map a_to_enum_b enum_a
  
let rec list_enumerator = function 
  | [] -> Seq.return []
  | enum_head :: others ->
     list_enumerator others >>= fun tail ->
     enum_head >>= fun head -> 
     Seq.return (head::tail)


let permutation_list_enumerator enum_list =
  permutation_enumerator enum_list >>= list_enumerator 
  


(* let rec take n seq = 
 *   match seq () with 
 *   | _ when n = 0 -> []          
 *   | Seq.Nil -> []
 *   | Seq.Cons (e,seq) -> e :: take (n-1) seq 
 * 
 * 
 * let _ = 
 *   subset_enumerator [1;2;3;4;5;6] |> List.of_seq |> List.length
 * 
 * let _ = 
 *   permutation_enumerator [1;2;3;4;5] |> List.of_seq |> List.length
 * 
 * let _ =
 *   pair_enumerator (List.to_seq [1;2;3;4]) (List.to_seq ['a';'b';'c']) |> List.of_seq
 * 
 * let _ = 
 *   list_enumerator [ List.to_seq [1;2;3]; List.to_seq [4;5]; List.to_seq [6]; List.to_seq [7;8] ] |> List.of_seq
 * 
 * let _ = 
 *   permutation_list_enumerator [ List.to_seq [1;2;3]; List.to_seq [4;5]; List.to_seq [6]; List.to_seq [7;8] ] |> List.of_seq *)
