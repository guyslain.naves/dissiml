open! DataStruct

let one = Big_int_Z.big_int_of_int 1 

module RND = FeatCore.RandomBigInt.Make(Feat.Num)(Random : FeatCore.RandomSig.S)

let factorials = 
  let open LazyList.Infinite in 
  let open Big_int_Z in 
  unfold (fun (n,f) -> (n+1, mult_big_int (big_int_of_int (n+1)) f)) (1,one) 
  |> map snd 

let factorial n = 
  LazyList.Infinite.get n factorials


let rec random_permutation = function
  | [] -> []
  | [ _ ] as l -> l 
  | list -> 
     let (left,right) = List.partition (fun _ -> Random.bool ()) list in
     List.rev_append (random_permutation left) (random_permutation right)


let choose choices = 
  let sum = 
    choices
    |> List.map snd 
    |>  List.fold_left Feat.Num.add Feat.Num.zero
  in 
  let r = RND.random sum in 
  let rec go r = function
    | (hd,k)::_ when r < k -> hd 
    | (_,k)::tail ->  go (Feat.Num.sub r k) tail 
    | [] -> assert false
  in  
  go r choices


let random_sublist list = 
  let l = List.length list in 
  let a = Random.int l in 
  let b = Random.int l in 
  let (mini,maxi) = (min a b, max a b) in 
  list
  |> MoreList.drop mini
  |> MoreList.take (maxi - mini + 1)

(* module GenSeries = 
 *   struct 
 * 
 *     type t =  
 *       int LazyList.Infinite.t
 *     
 *     open LazyList.Infinite
 *     open Big_int_Z 
 *        
 *     let (++) = Big_int_Z.add_big_int
 *     let ( ** ) = Big_int_Z.mult_big_int   
 * 
 * 
 *     let zero = unfold (fun _ -> zero_big_int) zero_big_int
 *     let one = unfold (fun _ -> zero_big_int) (big_int_of_int 1)
 *     let unit = lazy (Cons (zero_big_int, one))
 * 
 *     let from_general_form f =
 *       map f ints 
 * 
 *     let increment a = lazy (Cons (zero_big_int, a))
 * 
 *     let sum a b = zip_with (++) a b 
 * 
 *     let product a b = 
 *       ints 
 *       |> map (fun n -> 
 *              List.combine
 *                (a |> take (n + 1))
 *                (List.rev (b |> take (n + 1)))
 *              |> List.map (fun (a,b) -> a ** b) 
 *              |> List.fold_left (++) zero_big_int 
 *            )
 * 
 * 
 *     let list a = 
 *       unfold (fun (previous, _) ->
 *           let next = 
 *             a 
 *             |> drop 1
 *             |> take (List.length previous)
 *             |> List.combine previous
 *             |> List.map (fun (pi, aj) -> pi ** aj)
 *             |> List.fold_left (++) zero_big_int
 *           in 
 *           (next::previous, next)
 *         ) 
 *         ( [big_int_of_int 1], big_int_of_int 1)
 *       |> map snd 
 * 
 * 
 *     let non_empty_list a =
 *       product a (list a)
 * 
 *     let rec list_lb lb a =
 *       if lb = 0 then list a 
 *       else product a (list_lb (lb-1) a)
 * 
 * 
 *   end *)
