val factorial : int -> Big_int_Z.big_int


val random_permutation : 'elt list -> 'elt list

val random_sublist : 'elt list -> 'elt list 


val choose : ('elt * Big_int_Z.big_int) list -> 'elt 
