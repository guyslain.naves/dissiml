val int_enumerator : int -> int Seq.t 

val gray_code_enumerator : int Seq.t 

val subset_enumerator : 'elt list -> 'elt list Seq.t 

val heap_transposition_enumerator : (int * int) Seq.t

val permutation_enumerator : 'elt list -> 'elt list Seq.t 


val pair_enumerator : 'a Seq.t -> 'b Seq.t -> ('a * 'b) Seq.t 

val list_enumerator : 'e Seq.t list -> 'e list Seq.t 

val (>>=) : 'a Seq.t -> ('a -> 'b Seq.t) -> 'b Seq.t 

val permutation_list_enumerator : 'e Seq.t list -> 'e list Seq.t 

