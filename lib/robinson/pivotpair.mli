open DissimilarityLib

val partition :
  'elt Dissimilarity.t -> 'elt -> 'elt -> 
  ('elt list * 'elt list * 'elt list) 
  * 'elt list 
  * ('elt list * 'elt list * 'elt list) 
  * 'elt list 
  * ('elt list * 'elt list * 'elt list)
  * 'elt list * 'elt list

val find_blocks :
  'elt Dissimilarity.t -> 'elt -> 'elt -> 'elt list list option


val algo : 'elt Dissimilarity.t -> ('elt -> int) -> 'elt list option



