open DissimilarityLib.Dissimilarity
open PqTreeLib 


let quadruples list = 
  let rec go k = function 
    | x1::( (x2::_) as tail) -> 
       Seq.append 
         (k (x1,x2) tail)
         (go k tail)
    | _ -> Seq.empty 
  in
  go
    (fun xpair -> go (fun ypair _ -> Seq.return (xpair,ypair)))
    list 

let find_incompatible_triples diss order = 
  order
  |> quadruples
  |> Seq.filter_map 
       (fun ((x1,x2),(y1,y2)) -> 
         if diss.d x1 y1 > diss.d x1 y2 then 
           Some (x1, y1, y2)
         else if diss.d x2 y2 > diss.d x1 y2 then 
           Some (x1,x2,y2)
         else None 
       )
    
  
let tripartition f set   =
  let bigger,rest = List.partition (fun w -> f w > 0) set in 
  let equal,smaller = List.partition (fun w -> f w = 0) rest in 
  (bigger,equal,smaller)

let main_seq list =
  ( List.concat list ::
      ( DataStruct.MoreList.strict_factors list 
        |> List.map List.concat 
      )
  ) 
  |> List.filter (function [_] -> false | _ -> true)


let partition diss p q = 
  let d = diss.d in 
  let l, rest = List.partition (fun x -> d x q > max (d x p) (d p q)) diss.elements in 
  let m, rest = List.partition (fun x -> d p q > max (d p x) (d x q)) rest in 
  let r, rest = List.partition (fun x -> d p x > max (d p q) (d q x)) rest in 
  let x, rest = List.partition (fun x -> x = p || (d x q = d p q && d x p < d p q)) rest in
  let y, rest = List.partition (fun x -> x = q || (d p x = d p q && d x q < d p q)) rest in 
  let a_eq, a_circ = List.partition (fun x -> d x p = d p q) rest in 
  let ll, lm, lr = tripartition (fun x -> d x p - d p q) l in
  let ml, mm, mr = tripartition (fun x -> d x p - d x q) m in
  let rr, rm, rl = tripartition (fun x -> d x q - d p q) r in 
  ((ll,lm,lr), x, (ml,mm,mr), y, (rl,rm,rr), a_eq, a_circ)

let find_blocks diss p q = 
  let d = diss.d in 
  let l, rest = List.partition (fun x -> d x q > max (d x p) (d p q)) diss.elements in 
  let m, rest = List.partition (fun x -> d p q > max (d p x) (d x q)) rest in 
  let r, rest = List.partition (fun x -> d p x > max (d p q) (d q x)) rest in 
  let x, rest = List.partition (fun x -> x = p || (d x q = d p q && d x p < d p q)) rest in
  let y, rest = List.partition (fun x -> x = q || (d p x = d p q && d x q < d p q)) rest in 
  let a_eq, _a_circ = List.partition (fun x -> d x p = d p q) rest in 
  let _ll, lm, lr = tripartition (fun x -> d x p - d p q) l in
  (* let _ml, _mm, _mr = tripartition (fun x -> d x p - d x q) m in  *)
  let _rr, rm, rl = tripartition (fun x -> d x q - d p q) r in 
  let (@@) = List.rev_append in 
  let big = lm @@ lr @@ x @@ m @@ a_eq @@ y @@ rl @@ rm in 
  match lr = [], m = [], rl = [] with 
  | false, false, false -> None 
  | true, true, true -> 
     Some (main_seq [x @@ y @@ a_eq] @@ [x; y; big])
  | true, true, false ->
     Some (main_seq  [x @@ a_eq; y; rl] @@ [x; big])
  | true, false, true ->
     Some (main_seq [ x @@ m @@ y @@ a_eq] @@ [x; m; y; big])
  | true, false, false ->
     Some (main_seq [a_eq; x; m; y; rl] @@ [big])
  | false, true, true ->
     Some (main_seq [lr; x; y @@ a_eq] @@ [y; big])
  | false, true, false ->
     Some (main_seq [lr; x; a_eq; y; rl] @@ [big])
  | false, false, true ->
     Some (main_seq [lr; x; m; y; a_eq] @@ [big])



(* let discriminates p q x blocks =
 *   List.mem p blocks 
 *   && List.mem q blocks
 *   && not (List.mem x blocks)  *)

let pivot diss pqtree p q = 
  Option.map 
    (fun blocks -> 
      (* assert (List.exists (discriminates p q x) blocks); *)
      List.for_all ImperativeBL.(pqtree.insert) blocks
    )
    (find_blocks diss p q)
  


let algo (type elt) diss index =
  let module Canonical = 
    PqTree.MakeCanonical(struct type t = elt let compare a b = compare (index a) (index b) end)
  in
  let initial_pqtree = 
    PqTree.(P (List.map (fun l -> Leaf l) diss.elements))
  in
  let pqtree = ImperativeBL.get_algorithms initial_pqtree index in 
  let rec go () =
    let tree = pqtree.get () in 
    let order = PqTree.frontier tree in
    match find_incompatible_triples diss order () with
    | Seq.Nil -> Some order  
    | Seq.Cons ((x,_,z),_) -> 
       if pivot diss pqtree x z = Some true then 
         go () 
       else None
  in 
  go ()
    
