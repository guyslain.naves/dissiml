open DissimilarityLib 
open PqTreeLib 
open Dissimilarity

let find_compatible_order dissimilarity = 
  let insert_chain pqtree_option s = match pqtree_option with 
  | None -> None 
  | Some pqtree -> 
      PqChainInsertion.refine_by_distances s (fun u -> dissimilarity.d s u) pqtree 
  in
  let initial_tree = 
    PqTree.(P (List.map (fun x -> Leaf x) dissimilarity.elements))
  in 
  List.fold_left insert_chain (Some initial_tree) dissimilarity.elements 
  |> Option.map PqTree.frontier
