
type 'elt t = 
  { elements : 'elt list;
    d : 'elt -> 'elt -> int
  }

val is_compatible_order : 'elt t -> 'elt list -> bool

val shuffle : 'elt t -> 'elt t

val from_list : int list -> int t 

