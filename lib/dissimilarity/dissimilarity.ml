open! DataStruct


type 'elt t = 
  { elements : 'elt list;
    d : 'elt -> 'elt -> int 
  }


exception EarlyExit 

let is_compatible_order diss order =
  let arr = Array.of_list order in 
  let n = Array.length arr in 
  try 
    for i = 0 to n-1 do 
      for j = 0 to n-2 do 
        if (diss.d arr.(i) arr.(j+1) - diss.d arr.(i) arr.(j)) * (j - i) < 0 then 
          raise EarlyExit
      done;
    done;
    true
  with
  | EarlyExit -> false


let int_sqrt n =
  let rec go i square = 
    if n < square then i - 1
    else go (i+1) (square + 2 * i + 1)
  in 
  go 0 0

let from_list values = 
  let n = int_sqrt (List.length values) in 
  let array = Array.init n (fun _ -> Array.make n 0) in 
  let stack = ref values in 
  for i = 0 to n-1 do 
    for j = 0 to n-1 do 
      array.(i).(j) <- List.hd !stack;
      stack := List.tl !stack 
    done;
  done;
  let elements = MoreList.range 0 (n-1) in 
  let d i j = array.(i).(j) in 
  { elements; d }
  

let shuffle diss = 
  { diss with elements = DataStruct.MoreList.shuffle diss.elements }
