open Dissimilarity

module type IOSig = 
  sig 
    
    val pretty_printer : Format.formatter -> 'elt t -> unit 

    val from_channel : in_channel -> int t option

    val from_file : string -> int t option 

    val from_stdin : unit -> int t option 

    val to_channel : out_channel -> 'elt t -> unit

    val to_file : 'elt t -> string -> unit

    val to_stdout : 'elt t -> unit 

end


let bracket_pp fmt diss = 
  let pp_line fmt i =
    Format.fprintf fmt "@[<h>[%a]@]"
      (Format.pp_print_list
         ~pp_sep:(fun fmt () -> Format.pp_print_text fmt "; ")
         Format.pp_print_int
      )
      (List.map (diss.d i) diss.elements) 
    
  in 
  Format.fprintf fmt "@[<v 2>[%a]@]"
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.pp_print_text fmt ";\n")
       pp_line
    )
    diss.elements


let plain_pp fmt diss = 
  let pp_line fmt i =
    Format.fprintf fmt "@[<h>%a@]"
      (Format.pp_print_list
         ~pp_sep:(fun fmt () -> Format.pp_print_text fmt " ")
         Format.pp_print_int
      )
      (List.map (diss.d i) diss.elements) 
    
  in 
  Format.fprintf fmt "@[<v>%a@]"
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.pp_print_text fmt "\n")
       pp_line
    )
    diss.elements


let int = 
  let open Opal in 
  let zero = int_of_char '0' in 
  many1 digit >>= fun digits ->
  return (List.fold_left (fun sum digit -> 10 * sum + int_of_char digit - zero) 0 digits)


  
let bracket_parser =
  let open Opal in
  let obrack = lexeme (exactly '[') in 
  let cbrack = lexeme  (exactly ']') in 
  let comma = lexeme (exactly ';' <|> exactly ',') in 
  let line = between obrack cbrack (sep_by (lexeme int) comma) in 
  between obrack cbrack (sep_by line comma) >>= fun lines ->
  return (Dissimilarity.from_list (List.concat lines))


   
let plain_parser =
  let open Opal in
  many1 (lexeme int) >>= fun coefs -> 
  return (from_list coefs)


  
module MakeIO = 
  functor (P : sig 
             val parser : (char, int t) Opal.parser 
             val pp : Format.formatter -> 'elt t -> unit 
           end) -> 
  struct 

    let pretty_printer = P.pp


    let to_channel out_chan elt =
      Format.fprintf 
        (Format.formatter_of_out_channel out_chan)
        "%a" P.pp elt 

    let to_file elt filename = 
      to_channel (open_out filename) elt 

    let to_stdout elt   =
      to_channel stdout elt 


    let from_stream stream = 
      Opal.parse P.parser stream 

    let from_channel in_chan = 
      in_chan 
      |> Opal.LazyStream.of_channel
      |> from_stream 
      
      
    let from_file filename =
      let file = open_in filename in 
      let result = 
        file 
        |> Opal.LazyStream.of_channel
        |> from_stream 
      in 
      close_in file;
      result

    let from_stdin () =
      from_channel stdin
      
  end


module Plain = MakeIO(struct let parser = plain_parser let pp = plain_pp end)  

module Bracketed = MakeIO(struct let parser = bracket_parser let pp = bracket_pp end)
