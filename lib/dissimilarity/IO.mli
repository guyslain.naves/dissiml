open Dissimilarity

module type IOSig = 
  sig 
    
    val pretty_printer : Format.formatter -> 'elt t -> unit 

    val from_channel : in_channel -> int t option

    val from_file : string -> int t option 

    val from_stdin : unit -> int t option 

    val to_channel : out_channel -> 'elt t -> unit

    val to_file : 'elt t -> string -> unit

    val to_stdout : 'elt t -> unit 

end
  

module Plain : IOSig 

module Bracketed : IOSig
