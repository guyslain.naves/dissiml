This project contains implementations in Ocaml of algorithms for
recognition of Robinson dissimilarities, as well as random generators
of dissimilarities. The primary goal is to provide an implementation
of the algorithm proposed by Carmona, Chepoi, Naves and Préa. We
actually give two implementations, in imperativeCCNP.ml (a mostly
imperative implementation) and robinsonCCNP.ml (a more functional
implementation). The two implementations are equivalent, except for
their respective performance which slightly differs from one another.






0) Need OCaml installed. Easy way: 
sudo apt-get install opam
opam init
eval `opam env`
opam install utop dune core feat core_bench qcheck <add more packages to the list if necessary>


see https://ocaml.org/learn/tutorials/up_and_running.html

1) compile:
dune build

2) execute: 
cat matrices/mat1 | dune exec copoint 


Run test:
dune runtest


Run benchmark:
dune exec robench


Open a toplevel: (useful for testing algorithms)
dune utop
